import React, { useState, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Steps } from 'antd';
import { useIntl, history } from 'umi';
import img1 from '@/assets/1.png';
import img2 from '@/assets/2.png';
import img3 from '@/assets/3.png';
import img4 from '@/assets/4.png';
import styles from './index.less';
import { getProjects } from './Project/service';

const { Meta } = Card;
const { Step } = Steps;

const OverView= () => {
  const intl = useIntl();
  const HOST = window.location.origin;
  const [total, setTotal] = useState(0);

  const getData = async () => {
    const { code, data } = await getProjects();
    if (code === 0) setTotal(data.total);
  }

  useEffect(() => {
    getData();
  }, [])

  const stepArr = [
    {
      title: intl.formatMessage({ id: 'Data Manage', defaultMessage: '数据管理' }),
      desc: intl.formatMessage({ id: 'Data List', defaultMessage: '数据列表' }),
      img: img1,
      onClickFunc: () => window.open(`${HOST}/AIStudio/adhub/dataManage/dataSet/list`,  '_blank')
    },
    {
      title: intl.formatMessage({ id: 'Prject Manage', defaultMessage: '项目管理' }),
      desc: `${intl.formatMessage({ id: 'Prject Number', defaultMessage: '项目数' })}: ${total}`,
      img: img2,
      onClickFunc: () => history.push('/project/list')
    },
    {
      title: intl.formatMessage({ id: 'Model Factory', defaultMessage: '模型工厂' }),
      desc: intl.formatMessage({ id: 'Model List', defaultMessage: '模型列表' }),
      img: img3,
      onClickFunc: () => window.open(`${HOST}/AIStudio/apworkshop/models`, '_blank')
    },
    {
      title: intl.formatMessage({ id: 'Inference Center', defaultMessage: '推理中心' }),
      desc: intl.formatMessage({ id: 'Deployment', defaultMessage: '部署情况' }),
      img: img4,
      onClickFunc: () => window.open(`${HOST}/AIStudio/apflow`, '_blank')
    }
  ];
  return (
    <PageContainer>
      <div className={styles.overViewWrap}>
        <Card style={{ overflow: 'auto' }}>
          <h1>{intl.formatMessage({ id: 'The whole process of deep learning', defaultMessage: '深度学习使用全流程' })}</h1>
          <Steps>
            {stepArr.map(i => (
              <Step status="finish" title={
                <div onClick={() => i.onClickFunc()}>
                  <Card
                    hoverable
                    style={{ width: 200 }}
                    cover={<img alt={i.title} src={i.img} />}
                  >
                    <Meta title={i.title} description={i.desc} />
                  </Card>
                </div>}
              />
            ))}
          </Steps>
          {/* <h1 style={{ marginTop: 14 }}>使用详情</h1> */}
        </Card>
      </div>
    </PageContainer>
  );
};

export default OverView;
