/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState, useRef } from 'react';
import { PageContainer, PageLoading } from '@ant-design/pro-layout';
import { Button, Card, PageHeader, Modal, message, Menu, Select } from 'antd';
import { EllipsisTooltip, DetailTop, SelectProTitle } from '@/components';
import { UpdateUrlParam, getPageQuery } from '@/utils/utils';
import { ExclamationCircleOutlined, CaretDownOutlined, CloseOutlined } from '@ant-design/icons';
import { PAGEPARAMS } from '@/utils/const';
import { history, useIntl, connect } from 'umi';
import styles from './index.less';
import DevelopEnvForm from './DevelopEnvForm';
import DevelopEnvDetail from './DevelopEnvDetail';
import OtherEnvList from './OtherEnvList';
import { actionEnv, getEnvList } from './service';
import { getProjects, getProDetail } from '../Project/service';
import ModelTrain from './ModelTrain';
import GroupSourceTable from './GroupSourceTable';
import ModelDevelop from '../ModelDevelop';

const { confirm } = Modal;
const { Option } = Select;

const ExpertDevelop = ({ user }) => {
  const { formatMessage } = useIntl();
  const { userGroupId, userName } = user.currentUser;
  const [detail, setDetail] = useState({});
  const [allProData, setAllProData] = useState([]);
  const [pageLoading, setPageLoading] = useState(true);
  const [modalFlag, setModalFlag] = useState(false);
  const [selectProFlag, setSelectProFlag] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [step, setStep] = useState(1);
  const [envStatus, setEnvStatus] = useState(-1);
  const { proId } = getPageQuery();
  const envFormRef = useRef();
  const [activedId, setActivedId] = useState(0);
  const pageType = window.location.pathname === '/AIStudio/aiarts/project/expertDevelop' ? 'expert' : 'preset';

  const onGetProDetail = async (id) => {
    const { code, data } = await getProDetail(id || proId);
    if (code === 0 && data) {
      setDetail(data);
      UpdateUrlParam([{ name: 'labId', val: data.labId }]);
      if (data && data.type === 'preset') {
        setStep(2);
      } else {
        setEnvStatus(data.labStatus ? 1 : 0);
      }
    }
    setPageLoading(false);
  };

  const getData = async (pId) => {
    setPageLoading(true);
    if (!proId && !pId) {
      const { code, data } = await getProjects({
        ...PAGEPARAMS,
        type: pageType,
        name: userName
      });
      if (code === 0 && data) {
        const { items } = data;
        if (items.length) {
          UpdateUrlParam([{ name: 'proId', val: items[0].id }]);
          onGetProDetail(items[0].id);
        } else {
          const res = await getProjects({
            ...PAGEPARAMS,
            type: pageType
          });
          if (res.code === 0 && res.data && res.data.items.length) {
            UpdateUrlParam([{ name: 'proId', val: res.data.items[0].id }]);
            onGetProDetail(res.data.items[0].id);
          } else {
            setPageLoading(false);
            setModalFlag(true);
          }
        }
      }
    } else {
      onGetProDetail(pId);
    }
  };

  useEffect(() => {
    setActivedId(0);
    getData();
  }, [proId]);

  const onCheckUsed = (callBack, obj) => {
    confirm({
      title: `资源不足，是否排队？`,
      icon: <ExclamationCircleOutlined />,
      okText: '排队',
      okType: 'primary',
      cancelText: '取消',
      centered: true,
      onOk: async () => {
        const { code } = await actionEnv(proId, 'start', { config: obj });
        if (code === 0) callBack && callBack();
      },
      onCancel() {},
    });
  };

  const onStartEnv = () => {
    envFormRef.current.form.validateFields().then(async (values) => {
      setBtnLoading(true);
      const { imageSource, imageName, dataset, resource, nodeNumber, resourceType } = values;
      const datasetTemp = Array.isArray(dataset)
        ? dataset.filter((i) => i && i.key && i.key.trim())
        : [];
      const { resourceName, resourceNumber } = resource;
      const obj = {
        imageSource,
        resourceType,
        imageName:
          imageSource === 1
            ? `${JSON.parse(imageName).imageName}:${JSON.parse(imageName).version}`
            : imageName,
        dataSets: [],
        resourceInfo: {
          quotaId: JSON.parse(resourceName).id,
          node: nodeNumber,
          num: resourceNumber,
        },
        isSsh: true,
      };
      if (imageSource === 1) obj.imageId = JSON.parse(imageName).versionId;
      if (datasetTemp.length) obj.dataSets = datasetTemp.map((i) => JSON.parse(i.key));
      const { code } = await actionEnv(proId, 'start', { config: obj }, 1);
      const callBack = () => {
        message.success(formatMessage({ id: 'start success！', defaultMessage: '启动成功！' }));
        setActivedId(0);
        setEnvStatus(1);
      };
      if (code === 0) {
        callBack();
      } else if (code === 100200023) {
        onCheckUsed(callBack, obj);
      }
      setBtnLoading(false);
    });
  };

  const onClickGroupEnv = async () => {
    const { code, data } = await getEnvList(proId, { groupId: userGroupId });
    if (code === 0) {
      const { items, total } = data;
      if (total && items) {
        setActivedId(items[0].uid);
        setEnvStatus(2);
      } else {
        message.warning(
          formatMessage({
            id: 'There is no development environment in this group',
            defaultMessage: '该组内没有开发环境',
          }),
        );
      }
    }
  };

  const getEnvContent = () => {
    const commonObj = { envStatus, setEnvStatus, activedId, setActivedId };
    // 0是新增开发环境，1是点击启动，2是停止
    switch (envStatus) {
      case 0:
        return (
          <div className={styles.envWrap}>
            <Card
              className={`${styles.leftCard} adaptiveHeight`}
              title={
                <>
                  {formatMessage({ id: 'develop environment', defaultMessage: '开发环境' })}
                  <EllipsisTooltip
                    text={formatMessage({
                      id: 'In addition to the platform preset image, other images are used without verification',
                      defaultMessage: '除平台预置镜像外，其他镜像使用未经验证',
                    })}
                    isQuestion
                  />
                </>
              }
            >
              <DevelopEnvForm ref={envFormRef} activedId={activedId} />
              <Button type="primary" loading={btnLoading} onClick={onStartEnv}>
                {formatMessage({ id: 'start develop environment', defaultMessage: '启动开发环境' })}
              </Button>
            </Card>
            <Card
              className={`${styles.rightCard} adaptiveHeight`}
              title={formatMessage({ id: 'User group resource', defaultMessage: '所属用户组资源' })}
            >
              <GroupSourceTable
                btnText={formatMessage({
                  id: 'group develop environment',
                  defaultMessage: '组内开发环境',
                })}
                onClickFunc={onClickGroupEnv}
              />
            </Card>
          </div>
        );
      case 1:
        return <DevelopEnvDetail {...commonObj} />;
      case 2:
        return (
          <div className={styles.otherEnvWrap}>
            <div className={styles.listWrap}>
              <OtherEnvList detail={detail} {...commonObj} />
            </div>
            <div className={styles.detWrap}>
              <DevelopEnvDetail {...commonObj} />
            </div>
          </div>
        );
      default:
        break;
    }
  };

  const getContent = () => {
    if (detail && detail.type === 'preset') {
      return <ModelDevelop />;
    }
    return step === 2 ? <ModelTrain proId={proId} /> : getEnvContent();
  };

  if (pageLoading) return <PageLoading />;

  if (modalFlag) {
    return (
      <Modal
        title={formatMessage({ id: 'New Project', defaultMessage: '新建项目' })}
        visible={modalFlag}
        onCancel={() => history.push(`/project/list`)}
        destroyOnClose
        maskClosable={false}
        footer={[
          <Button type="primary" onClick={() => history.push(`/project/list?isOpen=true`)}>
            {formatMessage({ id: 'New Project', defaultMessage: '新建项目' })}
          </Button>,
        ]}
      >
        <p>
          {formatMessage({
            id: 'Please create a project first',
            defaultMessage: '请先到项目列表页创建一个项目',
          })}
          ！
        </p>
      </Modal>
    );
  }

  return (
    <div className={styles.expertDevelopWrap}>
      <PageContainer
        title={<SelectProTitle getData={getData} name={detail.name} type={detail.type} />}
      >
        <PageHeader
          ghost={false}
          title={formatMessage({ id: 'return project list', defaultMessage: '返回项目列表' })}
          onBack={() => history.push(`/project/list`)}
        />
        {detail && (
          <DetailTop data={detail} proId={proId} step={step} setStep={setStep} ismodelDevelop={pageType === 'preset'} />
        )}
        <div className={styles.content}>{getContent()}</div>
      </PageContainer>
    </div>
  );
};

export default connect(({ user }) => ({ user }))(ExpertDevelop);
