import React, { useEffect, useState, useRef } from 'react';
import { Descriptions, Button, Card, Modal, message } from 'antd';
import { EllipsisTooltip, TextCopyToClipboard, StatusTag } from '@/components';
import { PageLoading } from '@ant-design/pro-layout';
import ATable from '@apulis/table';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useIntl, connect } from 'umi';
import { getPageQuery, compareStatus, getMemoryNum } from '@/utils/utils';
import { pollInterval } from '@/utils/const';
import useInterval from '@/hooks/useInterval';
import styles from './index.less';
import ModalForm from './components/ModalForm';
import {
  getEnvDetail,
  actionEnv,
  getEndpoints,
  saveIamge,
  addEndpoints,
  deleteEndpoints,
} from '../service';
import { getProDetail } from '../../Project/service';

const { confirm } = Modal;

const DevelopEnvDetail = ({ setEnvStatus, envStatus, activedId = 0, user, setActivedId }) => {
  const { formatMessage } = useIntl();
  const [detail, setDetail] = useState(null);
  const [infoModalFlag, setInfoModalFlag] = useState(false);
  const [uploadModalFlag, setUploadModalFlag] = useState(false);
  const [saveModalFlag, setSaveModalFlag] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [jubBtnLoading, setJubBtnLoading] = useState(false);
  const formModalRef = useRef();
  const { proId } = getPageQuery();
  const [pageLoading, setPageLoading] = useState(true);
  const [stopBtnLoading, setStopBtnLoading] = useState(false);
  const [sshData, setSshData] = useState([]);
  const [portData, setPortData] = useState([]);
  const [portModalFlag, setPortModalFlag] = useState(false);
  const isRunning = detail && detail.status === 7;
  const isUser = detail && user.currentUser.id === detail.uid;
  // const isClosing = detail && (detail.status === 5 || detail.status === 6 || detail.status === 99);
  const isClosing = detail && detail.status === 99;

  const getEndpointsData = async (labId, runId) => {
    const { code, data } = await getEndpoints(proId, labId || detail.labId, runId || detail.runId);
    if (code === 0 && Array.isArray(data)) {
      const ssh = data.filter((i) => i.name === '$ssh');
      const portArr = data.filter((i) => !i.name.includes('$'));
      setSshData(
        ssh.map((i) => {
          return {
            ...i,
            ip: i.url ? i.url.split('@')[1] : '--',
            secret_key: window.atob(i.secret_key),
          };
        }),
      );
      setPortData(portArr);
    }
  };

  const getData = async (withLoading = true) => {
    setPageLoading(withLoading);
    const { code, data } = await getEnvDetail(proId, { uid: activedId });
    if (code === 0 && data) {
      const { status, labId, runId, uid } = data;
      user.currentUser.id === uid && status === 7 && getEndpointsData(labId, runId);
      const hasChange = compareStatus(detail, data);
      hasChange && setDetail(data);
    }
    setPageLoading(false);
  };

  useEffect(() => {
    setSshData([]);
    setPortData([]);
    ((envStatus === 2 && activedId) || envStatus !== 2) && getData();
  }, [activedId]);

  useInterval(() => {
    getData(false);
  }, pollInterval);

  const getDescArr = () => {
    const descArr = [
      {
        val: detail.imageName || '--',
        text: '环境镜像',
      },
      {
        val: detail.codePath || '--',
        text: formatMessage({ id: 'code folder', defaultMessage: '代码文件夹' }),
      },
      {
        val: detail.outputPath || '--',
        text: formatMessage({ id: 'output folder', defaultMessage: '输出文件夹' }),
      },
      {
        val: detail.userDataPath || '--',
        text: formatMessage({ id: 'personal folder', defaultMessage: '个人文件夹' }),
      },
      {
        val: detail.teamDataPath || '--',
        text: formatMessage({ id: 'shared folder', defaultMessage: '共享文件夹' }),
      },
    ];
    if (Array.isArray(detail.datasetPath)) {
      detail.datasetPath.forEach((i, idx) => {
        descArr.push({
          val: i,
          text: `${formatMessage({ id: 'dataset', defaultMessage: '数据集' })}${idx + 1}`,
        });
      });
    }
    return descArr;
  };

  const onDeletePort = (name) => {
    confirm({
      title: formatMessage({
        id: 'Are you sure to delete this port？',
        defaultMessage: '确定删除该端口吗？',
      }),
      icon: <ExclamationCircleOutlined />,
      okText: formatMessage({ id: 'Delete', defaultMessage: '删除' }),
      okType: 'danger',
      cancelText: formatMessage({ id: 'Cancel', defaultMessage: '取消' }),
      centered: true,
      onOk: async () => {
        const { code } = await deleteEndpoints(proId, detail.labId, detail.runId, name);
        if (code === 0) {
          message.success(formatMessage({ id: 'delete success', defaultMessage: '删除成功' }));
          getEndpointsData();
        }
      },
      onCancel() {},
    });
  };

  const sshColumns = [
    {
      title: formatMessage({ id: 'node IP', defaultMessage: '节点IP' }),
      dataIndex: 'ip',
    },
    {
      title: formatMessage({ id: 'SSH', defaultMessage: 'SSH命令' }),
      dataIndex: 'url',
      render: (i) => <TextCopyToClipboard text={i} />,
    },
    {
      title: formatMessage({ id: 'password', defaultMessage: '密码' }),
      dataIndex: 'secret_key',
      render: (i) => (isUser ? <TextCopyToClipboard text={i} /> : null),
    },
  ];
  const portColumns = [
    {
      title: formatMessage({ id: 'port name', defaultMessage: '端口名称' }),
      dataIndex: 'name',
    },
    {
      title: formatMessage({ id: 'port', defaultMessage: '端口号' }),
      dataIndex: 'port',
    },
    {
      title: formatMessage({ id: 'action', defaultMessage: '操作' }),
      key: 'action',
      render: (t, item) => {
        return (
          <>
            <Button type="link" onClick={() => window.open(item.url, '_blank')}>
              {formatMessage({ id: 'open', defaultMessage: '打开' })}
            </Button>
            <Button danger onClick={() => onDeletePort(item.name)} type="link">
              {formatMessage({ id: 'delete', defaultMessage: '删除' })}
            </Button>
          </>
        );
      },
    },
  ];

  const onActionEnv = async () => {
    if (envStatus === 1) {
      // 停止
      setStopBtnLoading(true);
      const { code } = await actionEnv(proId, 'stop', { runId: detail.runId });
      if (code === 0) {
        message.success(formatMessage({ id: 'submit success！', defaultMessage: '提交成功！' }));
      }
      setStopBtnLoading(false);
    } else {
      const { code, data } = await getProDetail(proId);
      if (code === 0) {
        if (data.labStatus > 0 && data.labStatus < 100) {
          message.warning(
            formatMessage({
              id: 'stop the existing development environment first',
              defaultMessage: '请先停止已有开发环境',
            }),
          );
        } else {
          setEnvStatus(0);
        }
      }
    }
  };

  const onUploadFile = () => {
    formModalRef.current.form.validateFields().then(() => {
      message.success(formatMessage({ id: 'submit success！', defaultMessage: '提交成功！' }));
      setUploadModalFlag(false);
    });
  };

  const onAddPort = () => {
    formModalRef.current.form.validateFields().then(async (values) => {
      const { name, port } = values;
      if (Array.isArray(portData)) {
        if (portData.find((i) => i.name === name)) {
          message.warning(
            formatMessage({
              id: 'this port name already exists',
              defaultMessage: '该端口名称已存在',
            }),
          );
          return;
        }
        if (portData.find((i) => i.port === port)) {
          message.warning(
            formatMessage({ id: 'this port already exists', defaultMessage: '该端口已存在' }),
          );
          return;
        }
      }
      const { code } = await addEndpoints(proId, detail.labId, detail.runId, values);
      if (code === 0) {
        getEndpointsData();
        setPortModalFlag(false);
        message.success(formatMessage({ id: 'add success', defaultMessage: '新增成功' }));
      }
    });
  };

  const onOpenJupyter = async () => {
    setJubBtnLoading(true);
    const { code, data } = await getEndpoints(proId, detail.labId, detail.runId);
    if (code === 0 && Array.isArray(data)) {
      const res = data.find((i) => i.name === '$jupyter');
      res && window.open(res.url, '_blank');
    }
    setJubBtnLoading(false);
  };

  const onSaveImage = () => {
    formModalRef.current.form.validateFields().then(async (values) => {
      setBtnLoading(true);
      const { code } = await saveIamge(proId, detail.labId, detail.runId, {
        imageName: `${values.imageName}:${values.tag}`,
        action: 'save',
      });
      if (code === 0) {
        message.success(formatMessage({ id: 'submit success', defaultMessage: '提交成功' }));
        setSaveModalFlag(false);
      }
      setBtnLoading(false);
    });
  };

  if (pageLoading) return <PageLoading />;

  const text2 = formatMessage({ id: 'instructions', defaultMessage: '使用说明' });
  const text3 = formatMessage({ id: 'Cancel', defaultMessage: '取消' });
  const text4 = formatMessage({ id: 'Confirm', defaultMessage: '确定' });
  const text5 = formatMessage({ id: 'upload file', defaultMessage: '上传文件' });
  const text6 = formatMessage({ id: 'save image', defaultMessage: '保存镜像' });

  const getAddBtn = () => {
    return (
      <Button
        type="primary"
        disabled={detail.status < 100}
        onClick={() => {
          setActivedId(0);
          setEnvStatus(0);
        }}
      >
        {formatMessage({ id: 'new develop environment', defaultMessage: '新建开发环境' })}
      </Button>
    );
  };
  const getResourceCard = () => {
    if (detail) {
      const { resourceType, resourceInfo } = detail;
      const { node, device, memory, cpu } = resourceInfo;
      const { deviceType, deviceNum, series } = device;
      return (
        <Card>
          资源规格={resourceType === 'CPU' ? resourceType : `${deviceType}-${series}`}-{cpu}核{getMemoryNum(memory)}
          ，设备数量={deviceNum}，调度节点数量={node}
        </Card>
      );
    }
    return null;
  };

  return (
    <Card className={`${styles.developEnvDetailWrap} adaptiveHeight`}>
      <div className={styles.topWrap}>
        <Descriptions
          column={2}
          title={
            <span>
              {detail.creator}
              <span className={styles.titleWrap}>
                <EllipsisTooltip
                  text={`${formatMessage({
                    id: 'develop environment',
                    defaultMessage: '开发环境',
                  })}（任务ID：${detail.runId}）`}
                />
              </span>
              <StatusTag status={detail.status} runId={detail.runId} />
            </span>
          }
        >
          {getDescArr().map((i) => (
            <Descriptions.Item label={i.text}>{i.val}</Descriptions.Item>
          ))}
        </Descriptions>
        {envStatus !== 2 && (
          <div className={styles.btnWrap}>
            <div>
              <Button
                type="primary"
                disabled={
                  !isRunning ||
                  (isRunning && detail.scratchStatus > 0 && detail.scratchStatus < 100)
                }
                onClick={() => setSaveModalFlag(true)}
              >
                {text6}
                {detail.scratchStatus > 0 && <StatusTag status={detail.scratchStatus} runId={detail.runId} />}
              </Button>
              <Button type="primary" disabled={!isRunning} onClick={() => setUploadModalFlag(true)}>
                {text5}
              </Button>
            </div>
            <div>
              <Button
                type="primary"
                loading={jubBtnLoading}
                disabled={!isRunning}
                onClick={onOpenJupyter}
              >
                {formatMessage({ id: 'start', defaultMessage: '启动' })} JupyterLab
              </Button>
              <Button type="primary" onClick={() => setInfoModalFlag(true)}>
                {text2}
              </Button>
            </div>
            <div>
              {getAddBtn()}
              <Button type="primary" onClick={() => setEnvStatus(2)}>
                {formatMessage({ id: 'group develop environment', defaultMessage: '组内开发环境' })}
              </Button>
            </div>
          </div>
        )}
      </div>
      {detail?.uid === user.currentUser.id && (
        <div className={styles.centerWrap}>
          <Card
            className={styles.sshWrap}
            title={
              <>
                SSH
                <EllipsisTooltip
                  text="通过指定命令行可以实现代码版本管理和创建模型训练"
                  isQuestion
                />
              </>
            }
          >
            <ATable
              columns={sshColumns}
              dataSource={sshData}
              rowKey="ssh"
              pagination={false}
              reloadIcon={false}
              size="small"
            />
          </Card>
          <Card
            className={styles.portWrap}
            title={
              <>
                交互式端口
                <EllipsisTooltip text="如果是分布式节点仅展示 Master 节点的可交互端口" isQuestion />
                {isUser && (portData ? portData.length < 10 : true) && isRunning && (
                  <Button
                    style={{ float: 'right' }}
                    type="primary"
                    onClick={() => setPortModalFlag(true)}
                  >
                    新增端口
                  </Button>
                )}
              </>
            }
          >
            <ATable
              columns={portColumns}
              dataSource={portData}
              pagination={false}
              reloadIcon={false}
              rowKey="port"
              size="small"
            />
          </Card>
        </div>
      )}
      {getResourceCard()}
      {envStatus === 1 && detail.status < 100 && !isClosing && (
        <Button
          type="primary"
          loading={stopBtnLoading}
          onClick={onActionEnv}
          style={{ margin: '24px 10px 0 0' }}
        >
          {formatMessage({ id: 'stop develop environment', defaultMessage: '停止开发环境' })}
        </Button>
      )}
      {envStatus === 2 && (
        <Button
          type="primary"
          loading={stopBtnLoading}
          onClick={onActionEnv}
          style={{ margin: '24px 10px 0 0' }}
        >
          {formatMessage({ id: 'copy develop environment', defaultMessage: '复制开发环境' })}
        </Button>
      )}
      {infoModalFlag && (
        <Modal
          title={text2}
          visible={infoModalFlag}
          onCancel={() => setInfoModalFlag(false)}
          destroyOnClose
          width={694}
          footer={[
            <Button type="primary" onClick={() => setInfoModalFlag(false)}>
              {text4}
            </Button>,
          ]}
        >
          <p>
            {formatMessage({
              id: '1',
              defaultMessage: '环境启动后，将会调度资源完成Jupyter环境和SSH通道的创建',
            })}
            ：<br />
            1、
            {formatMessage({
              id: '2',
              defaultMessage:
                '开发环境任务状态为“运行中”后，用户可以启动Jupyterlab窗口，一个开发环境只能启动一个窗口，运行在master节点上（如果申请的是分布式多节点资源），请在Jupyter工作窗中进行开发，可以上传文件到开发环境中，可以将整个master节点上的开发环境保存成一个镜像，镜像会保存到镜像中心中的所属空间，以便下次直接用。请按照平台提供的模型发布命令行进行操作，否则无法将模型发布到模型工厂，并在推理中心进行推理',
            })}
            。<br />
            2、
            {formatMessage({
              id: '3',
              defaultMessage:
                'SSH通道在开发环境任务状态为“运行中”后，即可看到SSH访问的信息，如果添加的是分布式多节点，只会提供master节点的访问通道',
            })}
            。<br />
            3、
            {formatMessage({
              id: '4',
              defaultMessage: '交互式端口是用于给开发环境开放额外端口用于其他访问',
            })}
            。<br />
            4、
            {formatMessage({
              id: '5',
              defaultMessage: '开发环境会生成6个可持久化目录（请用户遵守目录使用约定）',
            })}
            ：<br />
            &nbsp;&nbsp;&nbsp; －－code:
            {formatMessage({
              id: '6',
              defaultMessage:
                '项目代码开发目录的本地副本，代码采用git版本工具来管理，项目删除该目录会删掉',
            })}
            <br />
            &nbsp;&nbsp;&nbsp; －－outputs:
            {formatMessage({
              id: '7',
              defaultMessage: '存放项目用户私有输出文件，项目删除该目录会删掉',
            })}
            <br />
            &nbsp;&nbsp;&nbsp; －－userdata:
            {formatMessage({
              id: '8',
              defaultMessage: '存放用户私有文件，项目删除不会影响userdata目录',
            })}
            <br />
            &nbsp;&nbsp;&nbsp; －－teamdata:
            {formatMessage({
              id: '9',
              defaultMessage: '存放用户组共享文件，项目删除不会影响teamdata目录',
            })}
            <br />
            &nbsp;&nbsp;&nbsp; {`－－adhub/{name}/{Vn}:`}
            {formatMessage({
              id: '10',
              defaultMessage: '存放ADHub数据集列表，该目录为只读目录，项目删除不会影响ADHub目录',
            })}
            <br />
            &nbsp;&nbsp;&nbsp; －－conda:
            {formatMessage({
              id: '11',
              defaultMessage: '存放用户私有conda环境，项目删除不会影响conda目录',
            })}
            <br />
          </p>
        </Modal>
      )}
      {uploadModalFlag && (
        <Modal
          title={text5}
          visible={uploadModalFlag}
          onCancel={() => setUploadModalFlag(false)}
          destroyOnClose
          maskClosable={false}
          footer={[
            <Button onClick={() => setUploadModalFlag(false)}>{text3}</Button>,
            <Button type="primary" onClick={onUploadFile} loading={btnLoading}>
              {text4}
            </Button>,
          ]}
        >
          <ModalForm
            ref={formModalRef}
            labId={detail.labId}
            projectId={proId}
            setBtnLoading={setBtnLoading}
          />
        </Modal>
      )}
      {saveModalFlag && (
        <Modal
          title={text6}
          visible={saveModalFlag}
          onCancel={() => setSaveModalFlag(false)}
          destroyOnClose
          maskClosable={false}
          footer={[
            <Button onClick={() => setSaveModalFlag(false)}>{text3}</Button>,
            <Button type="primary" onClick={onSaveImage} loading={btnLoading}>
              {text4}
            </Button>,
          ]}
        >
          <ModalForm ref={formModalRef} setBtnLoading={setBtnLoading} type={2} />
        </Modal>
      )}
      {portModalFlag && (
        <Modal
          title={formatMessage({ id: 'new port', defaultMessage: '新增端口' })}
          visible={portModalFlag}
          onCancel={() => setPortModalFlag(false)}
          destroyOnClose
          maskClosable={false}
          footer={[
            <Button onClick={() => setPortModalFlag(false)}>{text3}</Button>,
            <Button type="primary" onClick={onAddPort} loading={btnLoading}>
              {text4}
            </Button>,
          ]}
        >
          <ModalForm ref={formModalRef} setBtnLoading={setBtnLoading} type={3} />
        </Modal>
      )}
    </Card>
  );
};

export default connect(({ user }) => ({ user }))(DevelopEnvDetail);
