import { Form, Radio, Input, InputNumber } from 'antd';
import React, { useState, useImperativeHandle, forwardRef, useMemo } from 'react';
import { MODULE_NAME } from '@/utils/const';
import { useIntl, connect } from 'umi';
import { PortReg, ImageNameReg } from '@/utils/reg';

import Uppy from '@uppy/core';
import { Dashboard } from '@uppy/react';
import '@uppy/dashboard/dist/style.css';
import Chinese from '@uppy/locales/lib/zh_CN';
import Tus from '@uppy/tus';

const ModalForm = forwardRef((props, ref) => {
  const [form] = Form.useForm();
  const { setFieldsValue } = form;
  const { formatMessage } = useIntl();
  const { setBtnLoading, type = 1, user, labId, projectId } = props;
  const [dirType, setDirType] = useState('code');
  const { id, userName, userGroupId } = user.currentUser;
  const option = [
    { label: 'code', value: 'code' },
    { label: 'userdata', value: 'userdata' },
    { label: 'teamdata', value: 'teamdata' },
  ];
  useImperativeHandle(ref, () => ({ form }));

  const uppy = useMemo(() => {
    const options = {
      meta: {
        projectId, labId, userGroupId, userName, dirType,
        moduleName: MODULE_NAME,
        userId: id,
        taskType: "aiarts_dev_upload_data"
      },
      restrictions: {
        maxNumberOfFiles: 1,
        allowedFileTypes: ['.zip', '.tar', '.gz'],
        maxFileSize: 2 * 1024 * 1024 * 1024
      },
      locale: Chinese,
    }
    return new Uppy(options).use(Tus, {
      formData: true,
      fieldName: 'file',
      endpoint: '/file-server/api/v1/files',
      chunkSize: 4 * 1024 * 1024, // 分片大小
    }).on('upload', (data) => {
      setBtnLoading(true);
    }).on('complete', async (result) => {
      setFieldsValue({ 'file': 'file' });
      setBtnLoading(false);
    }).on('error', (error) => {
      setBtnLoading(false);
    });
  }, [dirType]);

  const formItemLayout = {
    labelCol: {
      span: 4,
    }
  };

  const getContent = () => {
    switch (type) {
      case 1: 
        return (
          <>
            <Form.Item
              label={formatMessage({ id: 'upload directory', defaultMessage: '上传目录' })}
              name="dirType"
              rules={[{ required: true, message: formatMessage({ id: 'please select upload directory', defaultMessage: '请选择上传目录' })}]}
            >
              <Radio.Group
                onChange={e => setDirType(e.target.value)}
                options={option}
                optionType="button"
              />
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'upload file', defaultMessage: '上传文件' })}
              name="file"
              rules={[{ required: true, message: formatMessage({ id: 'please upload file', defaultMessage: '请上传文件' })}]}
            >
              <Dashboard
                width='100%'
                height='100%'
                note='Images up to 200×200px'
                uppy={uppy}
                locale={{
                  strings: {
                    dropPaste: formatMessage({ id: 'click %{browse} or drag the file here to upload. only files within 2G can be uploaded(only .zip, .tar, .tar.gz files can be uploaded)', defaultMessage: '点击%{browse}或将文件拖拽到这里上传，仅支持2G内文件上传。(只支持上传.zip .tar .tar.gz)' }),
                    browse: formatMessage({ id: 'upload', defaultMessage: '上传' }),
                    youCanOnlyUploadFileTypes:  formatMessage({ id: 'only .zip, .tar, .tar.gz files can be uploaded', defaultMessage: '只支持上传格式为.zip, .tar, .tar.gz的文件' }),
                  },
                }}
              />
            </Form.Item>
          </>
        );
      case 2:
        return (
          <>
            <Form.Item
              label={formatMessage({ id: 'image name', defaultMessage: '镜像名称' })}
              name="imageName"
              rules={[{ required: true, max: 20 }, { ...ImageNameReg }]}
            >
              <Input placeholder={formatMessage({ id: 'please enter image name', defaultMessage: '请输入镜像名称' })} />
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'tag number', defaultMessage: 'tag号' })}
              name="tag"
              rules={[{ required: true, max: 20 }, { ...ImageNameReg }]}
            >
              <Input placeholder={formatMessage({ id: 'please enter tag number', defaultMessage: '请输入tag号' })} />
            </Form.Item>
            <p>
              {formatMessage({ id: 'a', defaultMessage: '说明：将整个开发环境保存为镜像，而且只保存master节点上的开发环境。镜像将会保存到镜像中心所属用户组中。如果保存多次而且镜像名称一致，则会自动生成镜像版本。' })}
            </p>
          </>
        )
      case 3:
        return (
          <>
            <Form.Item
              label={formatMessage({ id: 'port name', defaultMessage: '端口名称' })}
              name="name"
              rules={[{ required: true, min: 2, max: 20 }, {...PortReg}]}
            >
              <Input placeholder={formatMessage({ id: 'please enter port name', defaultMessage: '请输入端口名称' })} />
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'port', defaultMessage: '端口号' })}
              name="port"
              rules={[{ required: true }]}
            >
              <InputNumber 
                style={{ width: "100%" }} 
                min={1025} max={65535} 
                placeholder={formatMessage({ id: 'port range is 1025~65535', defaultMessage: '端口范围为1025~65535' })} 
              />
            </Form.Item>
          </>
        )
      default: 
        break;
    }
  }

  return (
    <Form form={form} {...formItemLayout} initialValues={type === 1 ? { dirType } : { name: '' }}>
      {getContent()}
    </Form>
  );
});

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps, null, null, { forwardRef: true })(ModalForm);