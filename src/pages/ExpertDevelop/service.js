import request from '@/utils/request';

export async function getPublishedDatasets(params) {
  return await request(`/datasets`, {
    params: { ...params, pageNum: 1, pageSize: 999, isPublished: true }
  });
}

export async function getEnvDetail(id, params) {
  return await request(`/projects/${id}/code-lab`, { params });
}

export async function actionEnv(id, action, data, checkUsed) {
  return await request(`/projects/${id}/code-lab/${action}?checkUsed=${checkUsed}`, {
    method: 'POST',
    data: data
  });
}

export async function getEnvList(id, params) {
  return await request(`/projects/${id}/code-lab/list`, { params });
}

export async function getResourceQuotas(params) {
  return await request(`/resource-quotas`, { params });
}

export async function getQuotaLimits(params) {
  return await request(`/quota-limits`, { params });
}

export async function getPublicImage() {
  return await request(`/apharbor/images/listImageWithVersion`, { params: { pageNum: 1, pageSize: 999 }});
}

export async function getEndpoints(proId, labId, runId) {
  return await request(`/projects/${proId}/code-lab/${labId}/runs/${runId}/endpoints`);
}

export async function saveIamge(proId, labId, runId, data) {
  return await request(`/projects/${proId}/code-lab/${labId}/runs/${runId}/Image`, {
    method: 'POST',
    data: data
  });
}

export async function addEndpoints(proId, labId, runId, data) {
  return await request(`/projects/${proId}/code-lab/${labId}/runs/${runId}/endpoints`, {
    method: 'POST',
    data: data
  });
}

export async function deleteEndpoints(proId, labId, runId, name) {
  return await request(`/projects/${proId}/code-lab/${labId}/runs/${runId}/endpoints/${name}`, {
    method: 'DELETE'
  });
}

export async function getGroupResource(params) {
  return await request(`/user-group-resources`, { params });
}