import React, { useEffect, useState, useRef, useImperativeHandle, forwardRef } from 'react';
import { Form, Select, Radio, Input } from 'antd';
import { EllipsisTooltip, MultipleItems, ResourceForm } from '@/components';
import { PageLoading } from '@ant-design/pro-layout';
import { useIntl } from 'umi';
import { getPageQuery } from '@/utils/utils';
import { ExclamationCircleFilled } from '@ant-design/icons';
import { getPublishedDatasets, getEnvDetail, getPublicImage } from '../service';

const { Option } = Select;

const DevelopEnvForm = ({ activedId }, ref) => {
  const { formatMessage } = useIntl();
  const [pageLoading, setPageLoading] = useState(true);
  const [imageSource, setImageSource] = useState(1);
  const [resourceType, setResourceType] = useState('CPU');
  const [datasetOptions, setDatasetOptions] = useState([]);
  const [imageOptions, setIamgeOptions] = useState([]);
  const [dataSetsArr, setDataSetsArr] = useState([]);
  const [form] = Form.useForm();
  const { setFieldsValue } = form;
  const multipleItemRef = useRef();
  const { proId } = getPageQuery();
  useImperativeHandle(ref, () => ({ form }));

  const getData = async () => {
    setPageLoading(true);
    const res = await Promise.all([getPublishedDatasets(), getPublicImage()]);
    if (res.length) {
      if (res[0].code === 0) setDatasetOptions(res[0].data.cvDataset);
      if (res[1].code === 0) setIamgeOptions(res[1].data.items);

      if (activedId) {
        const { code, data } = await getEnvDetail(proId, { uid: activedId });
        if (code === 0) {
          const { imageName, imageId, dataSets } = data;
          const source = Number(data.imageSource);
          setResourceType(data.resourceType);
          setDataSetsArr(
            dataSets.map((i) => {
              return { key: JSON.stringify(i) };
            }),
          );
          setImageSource(source);
          setFieldsValue({
            imageSource: source,
            imageName:
              source === 1
                ? JSON.stringify(res[1].data.items.find((i) => i.versionId == imageId))
                : imageName,
            resourceType: data.resourceType,
          });
        }
      }
    }
    setPageLoading(false);
  };

  useEffect(() => {
    getData();
  }, []);

  const text1 = formatMessage({ id: 'please select image type', defaultMessage: '请选择镜像类型' });
  const text4 = formatMessage({ id: 'dataset', defaultMessage: '数据集' });

  const SelectFilter = (input, option) => {
    return option.children.props.text.toLowerCase().indexOf(input.toLowerCase()) >= 0;
  };

  const onImageSourcChange = (e) => {
    const val = e.target.value;
    setFieldsValue({ imageName: null });
    setImageSource(val);
  };

  if (pageLoading) return <PageLoading />;

  return (
    <Form form={form} initialValues={{ imageSource, resourceType }} labelAlign="left">
      <Form.Item
        name="imageSource"
        label={formatMessage({ id: 'image source', defaultMessage: '镜像来源' })}
        rules={[{ required: true }]}
      >
        <Radio.Group onChange={onImageSourcChange}>
          <Radio value={1}>
            {formatMessage({ id: 'image center', defaultMessage: '镜像中心' })}
          </Radio>
          <Radio value={2}>
            {formatMessage({ id: 'external image', defaultMessage: '外部镜像' })}
          </Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item
        name="imageName"
        className="speSelectItem"
        label="镜像名称"
        extra={<span><ExclamationCircleFilled style={{ marginRight: 6 }} />镜像中如果没有安装jupyter-lab，任务将会报错</span>}
        rules={[
          {
            required: true,
            message: imageSource === 1 ? text1 : "请输入镜像名称",
          },
          { pattern: /^(?!#).*/, message: '外部镜像不能以#开头' }
        ]}
      >
        {imageSource === 1 ? (
          <Select placeholder={text1} showSearch filterOption={SelectFilter} allowClear>
            {imageOptions.map((i) => (
              <Option key={JSON.stringify(i)} value={JSON.stringify(i)}>
                <EllipsisTooltip text={`${i.imageName}:${i.version}`} />
              </Option>
            ))}
          </Select>
        ) : (
          <Input
            placeholder={`${formatMessage({
              id: 'please enter public image access path in dockerhub',
              defaultMessage: '请输入dockerhub中的公开镜像访问路径',
            })}`}
          />
        )}
      </Form.Item>
      <Form.Item
        name="dataset"
        label={
          <>
            {text4}
            <EllipsisTooltip
              isQuestion
              text={formatMessage({
                id: 'import datasets from ADHub and add up to 5 datasets',
                defaultMessage: '从ADHub中导入数据集，最多添加5个',
              })}
            />
          </>
        }
      >
        <MultipleItems
          ref={multipleItemRef}
          form={form}
          data={dataSetsArr}
          propKey="dataset"
          typeText={text4}
          max={5}
          options={datasetOptions}
        />
      </Form.Item>
      <ResourceForm form={form} resourceTypeData={resourceType} />
    </Form>
  );
};

export default forwardRef(DevelopEnvForm);
