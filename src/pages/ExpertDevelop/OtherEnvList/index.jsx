import React, { useEffect, useState } from 'react';
import { Button, Card, Pagination } from 'antd';
import moment from 'moment';
import { getPageQuery, compareStatus } from '@/utils/utils';
import StatusTag from '@/components/StatusTag';
import { connect, useIntl } from 'umi';
import EllipsisTooltip from '@/components/EllipsisTooltip';
import useInterval from '@/hooks/useInterval';
import { PAGEPARAMS, TIMEFORMAT, pollInterval } from '@/utils/const';
import styles from './index.less';
import { getEnvList } from '../service';

const OtherEnvList = (props) => {
  const { setEnvStatus, user, activedId, setActivedId, detail } = props;
  const { id, userGroupId } = user.currentUser;
  const [listData, setListData] = useState({ items: [], total: 0 });
  const { proId } = getPageQuery();
  const [pageParams, setPageParams] = useState(PAGEPARAMS);
  const { formatMessage } = useIntl();

  const getData = async () => {
    const { code, data } = await getEnvList(proId, {
      groupId: userGroupId,
      ...pageParams,
    });
    if (code === 0 && data) {
      const { total, items } = data;
      const hasChange = compareStatus(listData.items, items);
      if (hasChange) setListData({ items, total });
      !activedId && items && items.length > 0 && setActivedId(items[0].uid);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  useInterval(() => {
    getData();
  }, pollInterval);

  const pageParamsChange = (page, size) => {
    setPageParams({ pageNum: page, pageSize: size });
  };

  const onClickMyEnv = (type = 1) => {
    setActivedId(type === 2 ? 0 : id);
    setEnvStatus(type === 2 ? 0 : 1);
  };

  return (
    <Card
      title={
        <h3>
          {formatMessage({ id: 'group develop environment', defaultMessage: '组内开发环境' })}
        </h3>
      }
      className={`${styles.otherEnvListWrap} adaptiveHeight`}
      extra={
        detail.labStatus ? (
          <Button type="primary" onClick={onClickMyEnv}>
            {formatMessage({ id: 'my develop environment', defaultMessage: '我的开发环境' })}
          </Button>
        ) : (
          <Button type="primary" onClick={() => onClickMyEnv(2)}>
            {formatMessage({ id: 'new develop environment', defaultMessage: '新建开发环境' })}
          </Button>
        )
      }
    >
      <div>
        {listData && listData.items && listData.items.length
          ? listData.items.map((i) => {
              const { creator, imageName, runId, createTime, status, uid } = i;
              return (
                <Card
                  className={`${styles.item} ${uid === activedId ? styles.actived : ''}`}
                  onClick={() => setActivedId(uid)}
                >
                  <p>
                    {creator}
                    <div style={{ float: 'right' }}><StatusTag status={status} runId={runId} /></div>
                  </p>
                  <p>
                    <EllipsisTooltip
                      text={`${formatMessage({ id: 'task', defaultMessage: '任务' })}ID：${runId}`}
                    />
                  </p>
                  <p>
                    <EllipsisTooltip
                      text={`${formatMessage({
                        id: 'image type',
                        defaultMessage: '镜像类型',
                      })}：${imageName}`}
                    />
                  </p>
                  <p>
                    {formatMessage({ id: 'createTime', defaultMessage: '创建时间' })}：
                    {moment(createTime).format(TIMEFORMAT)}
                  </p>
                </Card>
              );
            })
          : null}
      </div>
      {listData.total > 0 && (
        <Pagination
          total={listData.total}
          showTotal={(total) =>
            `${formatMessage({ id: 'total', defaultMessage: '共' })} ${total} ${formatMessage({
              id: 'items',
              defaultMessage: '条',
            })}`
          }
          onChange={pageParamsChange}
        />
      )}
    </Card>
  );
};

export default connect(({ user }) => ({ user }))(OtherEnvList);
