import React, { useState } from 'react';
import { Card, Steps, Table } from 'antd';
import TextCopyToClipboard from '@/components/TextCopyToClipboard';
import styles from './index.less';

const { Step } = Steps;

const ModelTrain = () => {
  const data = [
    { type: 'git add', info: '添加代码到缓存区（请在code目录下执行）' },
    { type: 'git commit -m “备注信息”', info: '将缓存区内容添加到本地仓库中' },
    { type: 'git push', info: '提交代码到 gitee 服务' }
  ];
  const data2 = [{ type: '/start/aplab -codePath {代码目录} -trainOutPutPath {训练输出路径} -modelName {模型名称}', info: '发布模型' }];
  const [datasource, setDatasource] = useState(data);
  const [current, setCurrent] = useState(0);
  const columns = [
    {
      title: '命令行',
      dataIndex: 'type',
      render: (i) => <TextCopyToClipboard text={i} />
    },
    {
      title: '说明',
      dataIndex: 'info',
    },
  ];

  const onChange = c => {
    setCurrent(c);
    setDatasource(c ? data2 : data);
  }

  return (
    <div className={styles.modelTrainWrap}>
      <Card className={`${styles.leftCard} adaptiveHeight`} title="训练说明">
        <p>用户可自定义进行训练和评估，但发布时需要按照平台提供的命令行进行发布，否则无法将模型发布到模型工厂，而且无法在推理中心推理该模型。</p>
      </Card>
      <Card className={`${styles.rightCard} adaptiveHeight`} title="发布命令">
        <div className={styles.stepWrap}>
          <Steps current={current} onChange={onChange}>
            <Step title="代码提交" />
            <Step title="模型发布" />
          </Steps>
        </div>
        <Table 
          columns={columns}
          dataSource={datasource}
          pagination={false}
        />
      </Card>
    </div>
  )
}

export default ModelTrain;