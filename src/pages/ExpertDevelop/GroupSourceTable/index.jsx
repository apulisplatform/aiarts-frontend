import React, { useEffect, useState, useRef } from 'react';
import { Button, message } from 'antd';
import ATable from '@apulis/table';
import { useIntl, connect } from 'umi';
import { getGroupResource } from '../service';

const GroupSourceTable = ({ onClickFunc, btnText, user }) => {
  const tableRef = useRef();
  const { organizationId, userGroupId } = user.currentUser;
  const { formatMessage } = useIntl();
  const [groupResource, setGroupResource] = useState([]);
  const [groupResourceLoading, setGroupResourceLoading] = useState(false);

  const reloadResource = async (isReload = true) => {
    setGroupResourceLoading(true);
    const { code, data } = await getGroupResource({ orgId: organizationId, userGroupId });
    if (code === 0 && data.items) {
      setGroupResource(data.items[0].quotas);
      isReload && message.success(formatMessage({ id: 'reload success', defaultMessage: '刷新成功' }));
    }
    setGroupResourceLoading(false);
  }

  useEffect(() => {
    reloadResource(false);
  }, []);

  const columns = [
    {
      title: formatMessage({ id: 'Resource Type', defaultMessage: '资源类型' }),
      dataIndex: 'type',
    },
    {
      title: formatMessage({ id: 'Resource Model', defaultMessage: '资源型号' }),
      dataIndex: 'resource model',
      render: (i, item) => <span>{item.type === 'CPU' ? item.arch : `${item.computeType}${item.series ? `-${item.series}` : ''}`}</span>
    },
    {
      title: "资源数量（可用/总数）",
      dataIndex: 'resource number',
      render: (i, item) => <span>{Number(item.num - item.used)}/{item.num}（{item.type === 'CPU' ? '核' : '个'}）</span>
    },
  ];

  return (
    <ATable
      columns={columns}
      actionRef={tableRef}
      rowKey="type"
      dataSource={groupResource}
      reloadIcon={false}
      loading={groupResourceLoading}
      leftToolBar={
        <>
          <Button type="primary" onClick={reloadResource}>{formatMessage({ id: 'reload resource', defaultMessage: '刷新资源' })}</Button>
          {onClickFunc && <Button type="primary" style={{ marginLeft: 10 }} onClick={onClickFunc}>{btnText}</Button>}
        </>
      }
    />
  )
}

export default connect(({ user }) => ({ user }))(GroupSourceTable);
