import request from '@/utils/request';

export async function getVisualizationList(proId, labId, runId, prefix) {
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}/files?prefix=${prefix || '/'}`);
}

export async function getVisualizationDetail(proId, labId, runId, prefix, isSpe) {
  const obj = {};
  if (isSpe) {
    obj.responseType = 'blob';
  }
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}/view?prefix=${prefix}&format=1`, obj);
}


/**
 * 获取评估指标
 * @param {*} proId
 * @param {*} labId
 * @param {*} runId
 * @param {string} name
 * @returns
 */
export async function getEvalParam(proId, labId, runId,name) {
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}/view?prefix=${name}&format=1`)
}
