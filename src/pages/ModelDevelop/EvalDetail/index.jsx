import React, { useEffect, useState, useRef } from 'react';
import {
  Button,
  Spin,
  Pagination,
  Empty,
  Collapse,
  Tabs,
  Table,
  Tooltip,
  Card,
} from 'antd';
import { PageLoading } from '@ant-design/pro-layout';
import _ from 'lodash';
import { history } from 'umi';
import { getPageQuery, getMemoryNum } from '@/utils/utils';
import ImageGallery from 'apulis-image-gallery';
import { CaretRightOutlined } from '@ant-design/icons';
import { getVisualizationList, getVisualizationDetail, getEvalParam } from './service';
import { fetchLogs, getTaskDetail } from '../service';
import styles from './index.less';
import DetailItem from '../DetailItem';

const { Panel } = Collapse;
const { TabPane } = Tabs;

const TaskDetail = (props) => {
  const { runId, download = true, onActionTrain, GoToTrain, isSelf, disabledRelease } = props;
  const [logs, setLogs] = useState([]);
  const [logLoading, setlogLoading] = useState(false);
  const [total, setTotal] = useState(1);
  const [current, setCurrent] = useState(1);
  const [startIndex, setStartIndex] = useState({});
  const [csvData, setCsvData] = useState({});
  const [loading, setLoading] = useState({});
  const [imgData, setImgData] = useState({});
  const [jsonData, setJsonData] = useState({})
  const [pageLoading, setPageLoading] = useState(true);
  const [tabKey, setTabKey] = useState('ok_images');
  const [activeKey, setActiveKey] = useState(['1']);
  const [detailData, setDetailData] = useState(null);
  const [descData, setDescdata] = useState(null);
  const imageGalleryRef = useRef();
  const { proId, labId } = getPageQuery();

  const getData = async () => {
    setPageLoading(true);
    const { code, data } = await getTaskDetail(proId, labId, runId);
    if (code === 0) {
      setDetailData(data);
      setDescdata(data.description);
      setPageLoading(false);
    }
  };

  const fetchJsonFile = async() => {
    const { code, data } = await getEvalParam(proId, labId, runId, 'eval_result.json')
    if (code === 0) setJsonData(data);
  }

  useEffect(() => {
    getData();
    fetchJsonFile();
  }, []);

  const getLogs = async () => {
    setlogLoading(true);
    const { code, data } = await fetchLogs(proId, labId, runId, { pageNum: current });
    if (code === 0) {
      const { items, hasNext } = data;
      setLogs(items);
      if (hasNext && !(current < total)) {
        setTotal(current + 1);
      }
    }
    setlogLoading(false);
  };

  useEffect(() => {
    getLogs();
  }, [current]);

  const getResourceCard = () => {
    if (detailData?.quota?.request) {
      const { node, request } = detailData.quota;
      const { cpu, memory, device } = request;
      const { computeType, deviceNum, deviceType, series } = device;
      return (
        <Card style={{ marginTop: '24px' }}>
          资源规格={computeType === 'CPU' ? computeType : `${deviceType}-${series}`}-{cpu}核{getMemoryNum(memory)}
          ，设备数量={deviceNum}，调度节点数量={node || 1}
        </Card>
      );
    }
    return null;
  };

  const getSpin = () => {
    return (
      <div style={{ textAlign: 'center' }}>
        <Spin />
      </div>
    );
  };

  const getCsvTable = (name) => {
    const key = name.split('.csv')[0];
    const data = csvData ? csvData[key] : null;
    if (!loading[key] && data && data.length) {
      const dataTemp = _.cloneDeep(data);
      const dataSource = [];
      const columnsTemp = data[0];
      const columns = columnsTemp.map((i) => {
        return {
          title: i,
          dataIndex: i,
        };
      });
      dataTemp.splice(1).forEach((i) => {
        const temp = {};
        i.forEach((n, idx) => {
          temp[columnsTemp[idx]] = n;
        });
        dataSource.push(temp);
      });
      return <Table columns={columns} dataSource={dataSource} pagination={false} />;
    }
    return loading[key] ? getSpin() : null;
  };

  const getJsonTable = () => {
    const columns = [
      {
        title: '名称',
        dataIndex: 'key'
      },
      {
        title: '值',
        dataIndex: 'value'
      },
      {
        title: '描述',
        dataIndex: 'desc'
      }
    ]
    if (jsonData.evaluation) {
      return <Table columns={columns} dataSource={jsonData.evaluation} pagination={false} />;
    }
    return null
  }

  const renderThumbInner = (args) => {
    const { thumbnail, thumbnailLabel } = args;
    return (
      <>
        <span className="image-gallery-thumbnail-inner">
          <img className="image-gallery-thumbnail-image" src={thumbnail} alt={thumbnailLabel} />
        </span>
        <Tooltip placement="bottomLeft" title={thumbnailLabel}>
          <div className="textEllipsis thumbnailTitle">{thumbnailLabel}</div>
        </Tooltip>
      </>
    );
  };

  const transformImgData = (data) => {
    return new Promise((resolve) => {
      // 通过FileReader转为base64
      const fr = new FileReader();
      fr.onload = (e) => {
        resolve(e.target.result);
      };
      fr.onerror = () => {
        resolve('');
      };
      fr.readAsDataURL(data);
    });
  };

  const onChangeLoading = (k, val) => {
    setLoading((pre) => {
      return {
        ...pre,
        [k]: val || false,
      };
    });
  };

  const getImgSrc = async (name, prefix) => {
    const res = await getVisualizationDetail(
      proId,
      labId,
      runId,
      `${prefix || tabKey}/${name}`,
      true,
    );
    return transformImgData(res).then((base64) => {
      return {
        original: base64,
        thumbnail: base64,
        thumbnailLabel: name,
        renderThumbInner: renderThumbInner,
      };
    });
  };

  const onSlide = (idx, key) => {
    const { urls, imgs } = imgData[key];
    const preLen = imgs.length;
    if (idx === preLen - 1 && idx !== urls.length - 1) {
      onChangeLoading(key, true);
      const tempData = urls.slice(preLen, preLen + 5);
      tempData.forEach(async (i) => {
        const temp = await getImgSrc(i.name, key);
        setImgData((pre) => {
          return {
            ...pre,
            [key]: {
              ...pre[key],
              imgs: pre[key].imgs.concat(temp),
            },
          };
        });
      });
      setStartIndex((pre) => {
        return {
          ...pre,
          [key]: preLen - 1,
        };
      });
      onChangeLoading(key, false);
    }
  };

  const getImgContent = (key) => {
    if (!Object.keys(loading).includes(key)) return null;
    if (!loading[key]) {
      if (imgData && imgData[key] && imgData[key].imgs.length) {
        return (
          <ImageGallery
            items={imgData[key].imgs}
            showPlayButton={false}
            showIndex
            ref={imageGalleryRef}
            startIndex={startIndex[key] ? startIndex[key] : 0}
            onSlide={(idx) => onSlide(idx, key)}
            total={imgData[key].urls.length}
          />
        );
      }
      return null;
    }
    return getSpin();
  };

  const getImages = async (name) => {
    onChangeLoading(name, true);
    const { code, data } = await getVisualizationList(proId, labId, runId, name);
    if (code === 0) {
      data.slice(0, 5).forEach(async (i) => {
        const temp = await getImgSrc(i.name, name);
        setImgData((pre) => {
          return {
            ...pre,
            [name]: {
              urls: data,
              imgs: pre && pre[name] && pre[name].imgs ? pre[name].imgs.concat(temp) : [temp],
            },
          };
        });
      });
    }
    onChangeLoading(name, false);
  };

  const onChangeResKey = (k) => {
    if (!imgData[k]) {
      getImages(k);
    }
    setTabKey(k);
  };

  const onCollapseChange = (k) => {
    if (k.includes('4')) getLogs();
    setActiveKey(k);
  };

  const onGetLogs = () => {
    if (!activeKey.includes('4')) {
      const keys = activeKey;
      keys.push('4');
      setActiveKey(keys);
    }
    getLogs({ pageNum: 1 });
  };

  const getItemArr = () => {
    const { tags, config, parent, num } = detailData;
    const { datasetName, datasetVersion, engine, modelName, modelVersionName } = tags;
    const itemArr = [
      { key: 'runId', label: '任务ID' },
      { key: 'parent',label: '训练任务', diyValue: `${parent} #${num}`},
      { key: 'modelName', label: '训练模型', diyValue: `${modelName}: ${modelVersionName}` },
      { key: 'datasetname', label: '数据集', diyValue: `${datasetName}: ${datasetVersion}` },
      { key: 'engine', label: '训练镜像', diyValue: engine },
      { key: 'userParams', label: '评估参数', diyValue: config.eval.userParams },
      { key: 'description', label: '评估备注' },
    ];
    return (
      <DetailItem 
        itemArr={itemArr} 
        proId={proId} 
        labId={labId} 
        runId={detailData.runId} 
        callBack={() => getData()}
        setDescdata={setDescdata}
        descData={descData}
        detailData={detailData}
      />
    )
  };

  if (pageLoading) return <PageLoading />;

  const hasActionBtn = (status) => {
    return (
      (status > 99 && isSelf) || (status < 100 && status !== 5 && status !== 6 && status !== 99)
    );
  };

  const onDownload = e => {
    e.stopPropagation();
    window.open(
      `/ai-arts/api/v1/projects/${proId}/labs/${labId}/runs/${runId}/fetch-logs?token=${localStorage.token}`,
    );
  }

  const onGetLogClick = e => {
    e.stopPropagation();
    onGetLogs();
  }

  return (
    <div className={styles.taskDetailWrap}>
      <Collapse
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        defaultActiveKey={['1']}
        activeKey={activeKey}
        onChange={onCollapseChange}
      >
        <Panel
          header="详情信息"
          key="1"
          extra={
            <>
              {hasActionBtn(detailData.status) && (
                <Button type="primary" danger onClick={() => onActionTrain(runId, 'kill')}>
                  {detailData.status > 99 ? '删除' : '停止'}
                </Button>
              )}
              <Button
                type="primary"
                style={{ margin: '0 16px' }}
                onClick={() => GoToTrain(runId, 2)}
              >
                复制
              </Button>
              <Button
                type="primary"
                onClick={() =>
                  history.push(
                    `/project/modelDevelop/release?proId=${proId}&labId=${labId}&runId=${runId}`,
                  )
                }
                disabled={disabledRelease(detailData.registerStatus, detailData.status)}
              >
                发布
              </Button>
            </>
          }
        >
          {getItemArr()}
        </Panel>
        <Panel header="评估指标" key="2">
          {getCsvTable('statistics.csv') || getJsonTable()}
        </Panel>
        {/* <Panel header="评估结果" key="3">
          <Tabs type="card" activeKey={tabKey} onChange={onChangeResKey}>
            <TabPane tab="已达标" key="ok_images">
              {getImgContent('ok_images')}
            </TabPane>
            <TabPane tab="未达标" key="ng_images">
              {getImgContent('ng_images')}
            </TabPane>
          </Tabs>
        </Panel> */}
        <Panel
          header="日志信息"
          key="4"
          extra={
            <>
              <Button type="primary" onClick={(e) => onGetLogClick(e)}>
                获取日志
              </Button>
              {download && logs && logs.length > 0 && (
                <Button
                  type="primary"
                  style={{ marginLeft: 24 }}
                  onClick={(e) => onDownload(e)}
                >
                  下载
                </Button>
              )}
            </>
          }
        >
          <Spin spinning={logLoading}>
            {logs && logs.length ? (
              <pre style={{ height: 800, background: '#eee' }}>{logs}</pre>
            ) : (
              <Empty description="暂无日志" />
            )}
          </Spin>
          {total ? (
            <Pagination
              pageSize={1}
              total={total}
              current={current}
              style={{ textAlign: 'right', margin: '24px 0' }}
              onChange={(p) => setCurrent(p)}
              showSizeChanger={false}
            />
          ) : null}
        </Panel>
      </Collapse>
      {getResourceCard()}
    </div>
  );
};

export default TaskDetail;
