import request from '@/utils/request';
import fileServerRequest from '@/utils/request-file';

export async function getTaskInfo(id, params) {
  return await request(`/projects/${id}/task-template`, {
    params
  });
}

export async function createTrain(proId, labId, data, checkUsed) {
  return await request(`/projects/${proId}/labs/${labId}/trains?checkUsed=${checkUsed}`, {
    method: 'POST',
    data: data
  });
}

export async function createEval(proId, labId, runId, data, checkUsed) {
  return await request(`/projects/${proId}/labs/${labId}/trains/${runId}/evals?checkUsed=${checkUsed}`, {
    method: 'POST',
    data: data
  });
}

export async function createRelease(proId, labId, runId, data) {
  return await request(`/projects/${proId}/labs/${labId}/trains/${runId}/register`, {
    method: 'POST',
    data: data
  });
}

export async function getTaskDetail(proId, labId, runId) {
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}`);
}

export async function deleteTask(proId, labId, runId) {
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}`, { method: 'DELETE' });
}

export async function actionTask(proId, labId, runId, action, data) {
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}?action=${action}`, {
    method: 'POST',
    data: data
  });
}

export async function fetchLogs(proId, labId, runId, params) {
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}/logs`, { params });
}

export async function getFileInfo(id) {
  return await fileServerRequest(`/files/info/${id}`);
}

export async function getYaml(modelId, modelVerId) {
  return await request(`/studioGetYaml/${modelId}/${modelVerId}`);
}

export async function editDesc(proId, labId, runId, data) {
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}`, {
    method: 'PUT',
    data: data
  });
}

export async function getGobTips(params) {
  return await request(`/misc/job-tips`, {
    params
  });
}
