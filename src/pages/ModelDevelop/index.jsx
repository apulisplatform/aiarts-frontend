import React, { useState } from 'react';
import { Button, Card } from 'antd';
import { useIntl, history } from 'umi';
import { getPageQuery, UpdateUrlParam } from '@/utils/utils';
import GroupSourceTable from '../ExpertDevelop/GroupSourceTable';
import TrainTable from './TrainTable';

const ModelDevelop = () => {
  const { formatMessage } = useIntl();
  const [isTable, setIsTable] = useState(getPageQuery().isTable);
  const { proId, labId } = getPageQuery();

  const onExtraClick = () => {
    if (isTable) {
      UpdateUrlParam([{ name: 'isTable', val: 0 }]);
      setIsTable(0);
    } else {
      history.push(`/project/modelDevelop/train?proId=${proId}&labId=${labId}&isTable=${isTable}`);
    }
  }

  return (
    <>
      <Card
        extra={
          <>
            <Button
              type="primary"
              onClick={() => onExtraClick()}
            >
              {isTable ? '所属用户组资源' : '新建训练任务'}
            </Button>
            {!isTable && <Button
              type="primary"
              style={{ marginLeft: 10 }}
              onClick={() => {
                UpdateUrlParam([{ name: 'isTable', val: 1 }]);
                setIsTable(1);
              }}
            >
              训练列表
            </Button>}
          </>
        }
        title={
          isTable
            ? "训练任务列表"
            : formatMessage({ id: 'User group resource', defaultMessage: '所属用户组资源' })
        }
      >
        {isTable ? <TrainTable /> : <GroupSourceTable />}
      </Card>
    </>
  );
};

export default ModelDevelop;
