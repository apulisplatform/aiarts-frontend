import React, { useEffect, useState, useMemo } from 'react';
import { PageContainer, PageLoading } from '@ant-design/pro-layout';
import { Button, Card, PageHeader, message, Form, Input, Checkbox, Descriptions, Popover } from 'antd';
import { history } from 'umi';
import { MODULE_NAME } from '@/utils/const';
import { SpeNameReg } from '@/utils/reg';
import { createRelease, getTaskDetail, getFileInfo, getYaml } from '../service';
import { getProDetail } from '../../Project/service';

import Uppy from '@uppy/core';
import { Dashboard } from '@uppy/react';
import '@uppy/dashboard/dist/style.css';
import Chinese from '@uppy/locales/lib/zh_CN';
import Tus from '@uppy/tus';

import { getPageQuery } from '@/utils/utils';
import styles from './index.less';

const { TextArea } = Input;

const Release = () => {
  const [form] = Form.useForm();
  const [releaseData, setReleaseData] = useState({});
  const [trainDataSet, setTrainDataSet] = useState('');
  const [proData, setProData] = useState({});
  const [btnLoading, setBtnLoading] = useState(false);
  const [inferCheckd, setInferCheckd] = useState(false);
  const [pageLoading, setPageLoading] = useState(true);
  const [maniFestData, setManiFestData] = useState(false);
  const { proId, labId, runId } = getPageQuery();
  const { setFieldsValue } = form;
  
  const formItemLayout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 9 },
  };

  const getData = async () => {
    setPageLoading(true);
    const res = await Promise.all([getTaskDetail(proId, labId, runId), getProDetail(proId)]);
    if (res.length) {
      if (res[0].code === 0) {
        setReleaseData({
          ...res[0].data,
          ...res[0].data.tags
        });
        if (res[0].data && res[0].data.tags) {
          const { modelId, modelVersion } = res[0].data.tags;
          const { code, data } = await getYaml(modelId, modelVersion);
          if (code === 0) {
            const val = data['infer.yaml'];
            setInferCheckd(Boolean(val));
            setManiFestData(val);
          }
        }
      };
      if (res[1].code === 0) setProData(res[1].data);
    }
    setPageLoading(false);
  }

  /**
   * 获取并设置训练的数据集
   */
  const getTrainData = async() => {
    if(releaseData.parent) {
      const { code, data } = await getTaskDetail(proId, labId, releaseData.parent)
      if(code === 0 && data && data.tags) {
        const { datasetName, datasetVersion } = data.tags
        setTrainDataSet(`${datasetName}: ${datasetVersion}`)
      }
    }
  }

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    getTrainData()
  }, [releaseData])

  const onRelease = () => {
    form.validateFields().then(async (values) => {
      setBtnLoading(true);
      const { code } = await createRelease(proId, labId, runId, {
        ...values,
        modelId: releaseData.tags.modelId,
        modelVersion: releaseData.tags.modelVersion,
        action: 'register'
      });
      if (code === 0) {
        history.push(`/project/modelDevelop?proId=${proId}&isTable=1&labId=${labId}&runId=${releaseData.parent}`)
        message.success('提交成功');
      }
      setBtnLoading(false);
    });
  }

  const getTextArr = () => {
    const textArr = [
      {
        key: 'modelType',
        text: '模型类型',
        val: `${proData.field}/${proData.task}`
      },
      {
        key: 'framework',
        text: '模型框架'
      },
      {
        key: 'a',
        text: '模型精度'
      },
      {
        key: 'backbone',
        text: '主干网络'
      },
      {
        key: 'engine',
        text: '容器镜像'
      },
      {
        key: 'trainDataSet',
        text: '训练数据集',
        val: `${trainDataSet}`
      },
      {
        key: 'evalDataset',
        text: '评估数据集',
        val: `${releaseData.datasetName}: ${releaseData.datasetVersion}`
      },
    ];

    return textArr.map(i =>
      <Descriptions.Item label={i.text} key={i.key} span={1}>
        {i.val || releaseData[i.key]}
      </Descriptions.Item>
    )
  }

  const uppy = useMemo(() => {
    const options = {
      meta: {
        moduleName: MODULE_NAME,
        taskType: "aiarts_dev_upload_data"
      },
      restrictions: {
        maxNumberOfFiles: 1,
        allowedFileTypes: ['.png', '.jpg', '.jpeg', '.gif', '.bmp'],
        maxFileSize: 2 * 1024 * 1024 * 1024
      },
      locale: Chinese,
    }
    return new Uppy(options).use(Tus, {
      formData: true,
      fieldName: 'file',
      endpoint: '/file-server/api/v1/files',
      chunkSize: 4 * 1024 * 1024, // 分片大小
    }).on('upload', (data) => {
      setBtnLoading(true);
    }).on('complete', async (result) => {
      const uploadURL = result.successful[0].uploadURL;
      const fileId = uploadURL.split('/').pop();

      // 根据 fileId 获取文件pvc路径，然后传给后端 http://yapi.apulis.cn:8444/project/518/interface/api/20934
      const { code, data } = await getFileInfo(fileId);
      if (code === 0) setFieldsValue({ 'logo': data.storagePath });
      setBtnLoading(false);
    }).on('file-removed', () => {
      setFieldsValue({ 'logo': null });
    }).on('error', (error) => {
      setBtnLoading(false);
    });
  }, []);

  if (pageLoading) return <PageLoading />;

  return (
    <div className={styles.releaseWrap}>
      <PageContainer>
        <PageHeader
          ghost={false}
          title={<span className="ant-page-header-heading-title">
            模型发布<span className={styles.grayInfo}>（模型发布是将开发训练好的模型发布到模型工厂进行保存，可以在模型工厂进行进一步处理）</span>
          </span>}
          onBack={() => history.push(`/project/modelDevelop?proId=${proId}&isTable=1&labId=${labId}`)}
        />
        <Card>
          <Form form={form} {...formItemLayout} initialValues={{ name: `${proData.name}-${releaseData.tags.modelName}` }}>
            <Form.Item name="name" label="模型名称"
              rules={[{ required: true, max: 50 },  {...SpeNameReg}]}
            >
              <Input placeholder="请输入模型名称" />
            </Form.Item>
            <Form.Item
              name="description"
              label="模型描述"
              rules={[{ max: 200 }]}
            >
              <TextArea placeholder="请输入模型描述" autoSize={{ minRows: 4 }} />
            </Form.Item>
            <Form.Item
              label="模型logo"
              name="logo"
            >
              <Dashboard
                width='100%'
                height='100%'
                note='Images up to 200×200px'
                uppy={uppy}
                locale={{
                  strings: {
                    dropPaste: '点击%{browse}或将文件拖拽到这里上传，仅支持上传图片',
                    browse: "上传",
                    youCanOnlyUploadFileTypes:  "只支持上传图片",
                    done: '重置'
                  },
                }}
              />
            </Form.Item>
            <Form.Item
              name="name" label="模型配置"
              className={styles.speItem}
            >
              {releaseData ? <Descriptions bordered size="middle">
                {getTextArr()}
              </Descriptions> : null}
            </Form.Item>
            <Form.Item
              name="infer" label="模型推理"
            >
              <Checkbox disabled checked={inferCheckd} />
              <span style={{ margin: '0 10px' }}>所选模型支持推理</span>
              {inferCheckd && <Popover 
                placement="right" 
                content={<div><pre>{maniFestData}</pre></div>} 
                trigger="click"
              >
                <a>推理manifest</a>
              </Popover>}
            </Form.Item>
          </Form>
          <Button style={{ marginTop: 16 }} loading={btnLoading} type="primary" onClick={onRelease}>发布</Button>
        </Card>
      </PageContainer>
    </div>
  )
}

export default Release;
