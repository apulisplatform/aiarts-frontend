/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-shadow */
import React, { useEffect, useState, useRef } from 'react';
import ATable from '@apulis/table';
import { Modal, Button, Input, Drawer, message, Tooltip } from 'antd';
import request from '@/utils/request';
import useInterval from '@/hooks/useInterval';
import { EllipsisTooltip, StatusTag } from '@/components';
import { ExclamationCircleOutlined, QuestionCircleFilled } from '@ant-design/icons';
import { useIntl, history, connect } from 'umi';
import { getPageQuery, delUrlParam } from '@/utils/utils';
import { pollInterval } from '@/utils/const';
import Tensorboard from '../Tensorboard';
import styles from './index.less';
import TrainDetail from '../TrainDetail';
import EvalDetail from '../EvalDetail';
import { deleteTask, actionTask, getGobTips } from '../service';

const { confirm } = Modal;

const TrainTable = ({ user }) => {
  const { formatMessage } = useIntl();
  const [viewDetail, setViewDetail] = useState({ runId: 0, status: 0, visible: false, type: 1, num: 1 });
  const tableRef = useRef();
  const expandTableRef = useRef();
  const { proId, labId, runId, backPageNum, backPageSize } = getPageQuery();
  const [expandedRows, setExpandedRows] = useState(runId ? [runId] : []);
  const typeText = viewDetail.type === 1 ? formatMessage({ id: 'train', defaultMessage: '训练' }) : formatMessage({ id: 'evaluation', defaultMessage: '评估' });

  useInterval(() => {
    tableRef.current && tableRef.current.reload(null, false);
  }, pollInterval);

  useInterval(() => {
    expandedRows.length && expandTableRef.current && expandTableRef.current.reload(null, false);
  }, pollInterval);

  useEffect(() => {
    if (tableRef.current && (backPageNum || backPageSize)) {
      tableRef.current.setPageParams(Number(backPageNum) || 1, Number(backPageSize) || 10);
      delUrlParam(['runId', 'backPageNum', 'backPageSize']);
    }
  }, [tableRef]);

  const onActionTrain = (id, action, type = 1) => {
    const actionText = {
      'kill': '停止',
      'delete': '删除',
      'cancel_register': '取消发布'
    }
    const text = actionText[action];
    confirm({
      title: `确定${text}吗？`,
      icon: <ExclamationCircleOutlined />,
      okText: text,
      okType: 'danger',
      cancelText: '取消',
      centered: true,
      onOk: async () => {
        const { code } = action === 'delete' ? await deleteTask(proId, labId, id) : await actionTask(proId, labId, id, action);
        if (code === 0) {
          if (type === 1) {
            tableRef.current.reload();
          } else {
            expandTableRef.current.reload();
          }
          message.success(`${text}成功`);
        }
      },
      onCancel() { }
    });
  }

  // const isClosing = (status) => {
  //   return status === 5 || status === 6 || status === 99;
  // }

  const disabledDelete = (userId, status) => {
    return userId !== user.currentUser.id || status < 100;
  }

  const disabledRelease = (registerStatus, status) => {
    return !(((registerStatus && registerStatus > 99) || !registerStatus) && status === 100);
  }

  const GoToTrain = (runId, type) => {
    const { pageNum, pageSize } = tableRef.current.pageParams;
    history.push(`/project/modelDevelop/train?proId=${proId}&labId=${labId}&runId=${runId}&type=${type}&backPageNum=${pageNum}&backPageSize=${pageSize}`);
  }

  const columns = [
    {
      title: '序号',
      dataIndex: 'num',
      render: (t, item) => <div
        style={{ color: '#4168AA', cursor: 'pointer' }}
        onClick={() =>
          setViewDetail({
            status: item.status,
            runId: item.runId,
            visible: true,
            type: 1 ,
            num: item.num
          })
        }
      >{t}</div>
    },
    {
      title: `训练状态`,
      dataIndex: 'status',
      render: (s, item) => <StatusTag status={s} runId={item.runId} />,
    },
    {
      title: '训练可视化',
      key: 'visual',
      width: 164,
      render: (text, item) => {
        return item.tags.visualization ? <Tensorboard runInfo={{ ...item, proId }} /> : <span>--</span>;
      },
    },
    {
      title: '训练模型',
      dataIndex: 'modelName',
      render: (i, item) => <EllipsisTooltip text={`${item.tags.modelName}:${item.tags.modelVersionName}`} />
    },
    {
      title: '数据集',
      dataIndex: 'dataSets',
      render: (i, item) => <EllipsisTooltip text={`${item.tags.datasetName}: ${item.tags.datasetVersion}`} />
    },
    {
      title: '容器镜像',
      dataIndex: 'engine',
      render: (i, item) => <EllipsisTooltip text={item.tags.engine} />
    },
    {
      title: formatMessage({ id: 'Train Description', defaultMessage: '训练备注' }),
      dataIndex: 'description',
      render: (i) => <EllipsisTooltip text={i} />
    },
    {
      title: formatMessage({ id: 'Creator', defaultMessage: '创建人' }),
      dataIndex: 'creator',
      render: (i) => <EllipsisTooltip text={i} />
    },
    {
      title: formatMessage({ id: 'Create Time', defaultMessage: '创建时间' }),
      key: 'createdAt',
      dataIndex: 'createdAt',
      format: 'time',
      sortable: true,
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      render: (t, item) => {
        const { status, runId, userId } = item;
        return (
          <div className={styles.actionWrap}>
            <Button type="link" disabled={status !== 100} onClick={() => GoToTrain(runId, 2)}>评估</Button>
            <Button type="link" onClick={() => GoToTrain(runId, 1)}>复制</Button>
            <Button type="link" disabled={status > 99} onClick={() => onActionTrain(runId, 'kill')}>停止</Button>
            <Button danger type="link" disabled={disabledDelete(userId, status)} onClick={() => onActionTrain(runId, 'delete')}>删除</Button>
          </div>
        );
      }
    }
  ];

  const expandedRowRender = (props) => {

    const GoToRelease = (runId) => {
      const { pageNum, pageSize } = tableRef.current.pageParams;
      history.push(`/project/modelDevelop/release?proId=${proId}&labId=${labId}&runId=${runId}&backPageNum=${pageNum}&backPageSize=${pageSize}`);
    }

    const onCancelRelease = (runId) => {

    }

    const expandColumns = [
      {
        title: '评估序号',
        dataIndex: 'num',
        render: (t, item) => <div
          style={{ color: '#4168AA', cursor: 'pointer' }}
          onClick={() => 
            setViewDetail({
              status: item.status,
              runId: item.runId,
              visible: true,
              type: 2,
              num: item.num
            })
          }
        >{t}</div>
      },
      {
        title: `评估状态`,
        dataIndex: 'status',
        render: (s, item) => <StatusTag status={s} runId={item.runId} />,
      },
      {
        title: '数据集',
        dataIndex: 'dataSets',
        render: (i, item) => <EllipsisTooltip text={`${item.tags.datasetName}: ${item.tags.datasetVersion}`} />
      },
      // {
      //   title: '模型精度mAP',
      //   dataIndex: 'a',
      // },
      {
        title: `发布状态`,
        dataIndex: 'registerStatus',
        render: s => {
          let text = !s || s === 0 ? '未发布' : '';
          if (s === 100) text = '发布成功';
          if (s === 104) text = '发布失败';
          return <StatusTag status={s} text={text} />
        },
      },
      {
        title: '评估备注',
        dataIndex: 'description',
        render: (i) => <EllipsisTooltip text={i} />
      },
      {
        title: formatMessage({ id: 'Create Time', defaultMessage: '创建时间' }),
        key: 'createdAt',
        dataIndex: 'createdAt',
        format: 'time',
        sortable: true,
      },
      {
        title: '操作',
        key: 'action',
        fixed: 'right',
        render: (t, item) => {
          const { status, runId, userId, registerStatus } = item;
          return (
            <div className={styles.actionWrap}>
              {registerStatus > 0 && registerStatus < 100 ? <Button type="link"
                onClick={() => onActionTrain(runId, 'cancel_register', 2)}
              >
                取消发布
              </Button> :
              <Button type="link"
                onClick={() => GoToRelease(runId)}
                disabled={disabledRelease(registerStatus, status)}
              >
                发布
              </Button>}
              <Button type="link" onClick={() => GoToTrain(props.runId, 2)}>复制</Button>
              <Button type="link" disabled={status > 99} onClick={() => onActionTrain(runId, 'kill', 2)}>停止</Button>
              <Button disabled={disabledDelete(userId, registerStatus)} danger type="link" onClick={() => onActionTrain(runId, 'delete', 2)}>删除</Button>
            </div>
          );
        }
      }
    ];

    return (
      <ATable
        dataSourceAPIPath={`/projects/${proId}/labs/${labId}/trains/${props.runId}/evals`}
        columns={expandColumns}
        request={request}
        actionRef={expandTableRef}
        reloadIcon={false}
        scroll={{ x: 1280 }}
        rowKey="runId"
        sticky
      />
    )
  }

  const drawerConfig = {
    closeIcon: <Button type="primary">关闭</Button>,
    mask: true,
    width: '75%'
  }

  const getViewDetail = () => {
    const obj = {
      onActionTrain, GoToTrain,
      runId: viewDetail.runId,
      isSelf: viewDetail.userId === user.currentUser.id
    };
    if (viewDetail.type === 2) {
      obj.disabledRelease = disabledRelease;
      return <EvalDetail {...obj} />;
    }
    return <TrainDetail {...obj} />;
  }

  return (
    <div className={styles.trainTableWrap}>
      <ATable
        dataSourceAPIPath={`/projects/${proId}/labs/${labId}/trains`}
        columns={columns}
        request={request}
        actionRef={tableRef}
        reloadIcon={false}
        leftToolBar={
          <Button
            type="primary"
            onClick={() =>
              history.push(
                `/project/modelDevelop/train?proId=${proId}&labId=${labId}`,
              )
            }
          >
            新建训练任务
          </Button>
        }
        scroll={{ x: 1280 }}
        rowKey="runId"
        sticky
        expandable={{
          rowExpandable: () => true,
          expandedRowRender,
          onExpandedRowsChange: r => setExpandedRows(r[r.length - 1] ? [r[r.length - 1]] : []),
          expandedRowKeys: expandedRows
        }}
      />
      {viewDetail.visible && <Drawer
        title={<div className="ant-drawer-title">{`#${viewDetail.num}`} {typeText}详情 <StatusTag runId={viewDetail.runId} status={viewDetail.status} /></div>}
        visible={viewDetail.visible}
        onClose={() => setViewDetail({ runId: 0, status: 0, visible: false })}
        {...drawerConfig}
        className="detailDrawerWrap"
      >
        {getViewDetail()}
      </Drawer>}
    </div>
  )
}

export default connect(({ user }) => ({ user }))(TrainTable);
