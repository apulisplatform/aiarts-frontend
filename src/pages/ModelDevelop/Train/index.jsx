import React, { useEffect, useState, useRef } from 'react';
import { PageContainer, PageLoading } from '@ant-design/pro-layout';
import {
  Button,
  Card,
  PageHeader,
  Modal,
  message,
  Form,
  Input,
  Checkbox,
  Select,
  Radio,
} from 'antd';
import { history, useIntl } from 'umi';
import { getPageQuery } from '@/utils/utils';
import ATable from '@apulis/table';
import request from '@/utils/request';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { EllipsisTooltip, RuningParamsItem, ResourceForm, SelectProTitle } from '@/components';
import styles from './index.less';
import { getTaskInfo, createTrain, getTaskDetail, createEval } from '../service';
import { getPublishedDatasets } from '../../ExpertDevelop/service';
import { getProDetail, getProjects } from '../../Project/service';

const { Option } = Select;
const { confirm } = Modal;

const Train = () => {
  const { formatMessage } = useIntl();
  const [form] = Form.useForm();
  const { setFieldsValue } = form;
  const [datasetArr, setDatasetArr] = useState([]);
  const [taskData, setTaskData] = useState({});
  const [proData, setProData] = useState({});
  const [useNNI, setUseNNI] = useState(false);
  const [selectModalFlag, setSelectModalFlag] = useState(false);
  const [selectRow, setSelectRow] = useState([]);
  const [expandedRows, setExpandedRows] = useState([]);
  const [checkKeys, setCheckKeys] = useState({});
  const [btnLoading, setBtnLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const { proId, type = '1', runId, labId, backPageSize, backPageNum } = getPageQuery(); // 1: 训练，2：评估
  const typeText =
    type === '1'
      ? formatMessage({ id: 'train', defaultMessage: '训练' })
      : formatMessage({ id: 'evaluation', defaultMessage: '评估' });
  const runningParamsRef = useRef();
  const [allProData, setAllProData] = useState([]);

  const getData = async (pId) => {
    setPageLoading(true);
    const res = await getProDetail(pId || proId);
    if (res.code === 0 && res.data) {
      if (res.data.type === 'preset') {
        const proRes = await getProjects({
          pageSize: 9999,
          pageNum: 1,
          type: 'preset'
        });
        if (proRes.code === 0 && proRes.data) setAllProData(proRes.data.items);
      }
      setProData(res.data);
      const res1 = await getPublishedDatasets({ field: res.data.field, task: res.data.task });
      if (res1.code === 0) setDatasetArr(res1.data.cvDataset);
      if (runId) {
        const res2 = await getTaskDetail(pId || proId, labId, runId);
        if (res2.code === 0) {
          const { tags, config, num } = res2.data;
          const {
            datasetId,
            modelId,
            modelName,
            modelVersion,
            modelVersionName,
            backbone,
            framework,
            engine,
            visualization,
          } = tags;
          const { code, data } = await getTaskInfo(pId || proId, {
            modelId,
            modelVersionId: modelVersion,
          });
          if (code === 0) {
            const obj = { modelName, modelId, modelVersionName };
            const { tasks, devices } = data;
            if (type === '2') {
              // 评估
              setTaskData({
                ...data,
                ...obj,
                num, 
                userParams: tasks.eval.userParams, 
              });
            } else {
              // 复制
              setTaskData({
                ...res2.data,
                ...obj,
                userParams: config.train.userParams, 
                versionId: modelVersion, 
                devices
              });
            }
          }
          setFieldsValue({
            dataSets: JSON.stringify(res1.data?.cvDataset?.find((i) => i.id === Number(datasetId))),
            backbone,
            framework,
            engine,
            visualization,
            modelName,
          });
        }
      }
    }
    setPageLoading(false);
  };

  useEffect(() => {
    getData();
  }, []);

  const lineTitle = (title) => {
    return (
      <div className={styles.lineTitle}>
        <div className={styles.line} />
        <h3>{title}</h3>
      </div>
    );
  };

  const getCheckBox = (key, label, text) => {
    return (
      <div className="checkBoxWrap">
        <Checkbox
          disabled
          defaultChecked
          onChange={(e) => setCheckKeys({ ...checkKeys, [key]: e.target.checked })}
        />
        <span>{label}</span>
        {text ? <EllipsisTooltip isQuestion text={text} /> : null}
      </div>
    );
  };

  const SelectFilter = (input, option) => {
    return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
  };

  const getParamsItem = () => {
    if (taskData && taskData.userParams && taskData.userParams.length) {
      return (
        <Form.Item label="运行参数" name="userParams" style={{ marginBottom: 0 }}>
          <RuningParamsItem ref={runningParamsRef} form={form} data={taskData.userParams} />
        </Form.Item>
      );
    }
    return null;
  };

  const onCheckUsed = (callBack, obj) => {
    confirm({
      title: `资源不足，是否排队？`,
      icon: <ExclamationCircleOutlined />,
      okText: '排队',
      okType: 'primary',
      cancelText: '取消',
      centered: true,
      onOk: async () => {
        const { code } =
        type === '1'
          ? await createTrain(proId, labId, obj)
          : await createEval(proId, labId, runId, obj);
        if (code === 0) callBack && callBack();
      },
      onCancel() {},
    });
  };

  const onCreatTask = () => {
    form.validateFields().then(async (values) => {
      setBtnLoading(true);
      const { dataSets, resource, nodeNumber, resourceType, userParams, useNNI, modelName } = values;
      const { resourceName, resourceNumber } = resource;
      const dataSetsTemp = JSON.parse(dataSets);
      const obj = {
        resourceType,
        modelName,
        userParams: taskData.userParams,
        datasetId: dataSetsTemp.datasetId,
        datasetVersion: dataSetsTemp.version,
        datasetName: dataSetsTemp.name,
        quota: {
          quotaId: JSON.parse(resourceName).id,
          node: nodeNumber,
          num: resourceNumber,
        },
        modelId: taskData.modelId,
        modelVersion: taskData.versionId
      };
      if (type === '1') {
        obj.useNNI = useNNI;
        obj.modelId = selectRow.length ? selectRow[0].modelId : taskData.modelId;
        obj.modelVersion = selectRow.length ? selectRow[0].versionId : taskData.versionId;
      }
      if (Array.isArray(userParams) && userParams.length) {
        userParams.forEach((i, idx) => {
          obj.userParams[idx].value = i.value;
        });
      }
      if (type === '2') obj.quota.node = 1;
      const { code } =
        type === '1'
          ? await createTrain(proId, labId, obj, 1)
          : await createEval(proId, labId, runId, obj, 1);
      const callBack = () => {
        message.success(formatMessage({ id: 'submit success', defaultMessage: '提交成功' }));
        history.push(`/project/modelDevelop?proId=${proId}&isTable=1&labId=${labId}&runId=${runId}&backPageNum=${backPageNum}&backPageSize=${backPageSize}`);
      }
      if (code === 0) {
        callBack();
      } else if (code === 100200023) {
        onCheckUsed(callBack, obj);
      }
      setBtnLoading(false);
    });
  };

  const columns = [
    {
      title: `模型名称`,
      dataIndex: 'name',
    },
    {
      title: `模型框架`,
      dataIndex: 'framework',
    },
    {
      title: `网络结构`,
      dataIndex: 'backbone',
    },
    {
      title: `版本数`,
      dataIndex: 'versionCount',
    },
    {
      title: `来源`,
      dataIndex: 'source',
      render: (i) => <span>{i === 'preset' ? '预置' : '发布'}</span>,
    },
    {
      title: '更新时间',
      key: 'createdAt',
      dataIndex: 'createdAt',
      format: 'time',
      sortable: true,
    },
  ];

  const expandedRowRender = ({ id, name }) => {
    const expandColumns = [
      {
        title: `版本`,
        dataIndex: 'name',
      },
      {
        title: `容器镜像`,
        dataIndex: 'engine',
      },
      // {
      //   title: `运行参数`,
      //   dataIndex: 'parameters',
      //   render: (i) => <EllipsisTooltip text={i} />,
      // },
      {
        title: `数据集`,
        dataIndex: 'datasetName',
      },
      {
        title: formatMessage({ id: 'Create Time', defaultMessage: '创建时间' }),
        key: 'createdAt',
        dataIndex: 'createdAt',
        format: 'time',
        sortable: true,
      },
    ];

    return (
      <ATable
        dataSourceAPIPath={`/studioModelVersionsByModel/${id}`}
        columns={expandColumns}
        request={request}
        scroll={{ x: 1280 }}
        sticky
        reloadIcon={false}
        rowKey="versionId"
        pagination={{
          size: 'small',
        }}
        rowSelection={{
          type: 'radio',
          onSelect: (record, selected, selectedRows) =>
            setSelectRow([{ ...selectedRows[0], name }]),
        }}
      />
    );
  };

  const onCloseModal = () => {
    setSelectModalFlag(false);
    setExpandedRows([]);
  };

  const onSelectModel = async () => {
    if (selectRow && selectRow.length) {
      const { modelId, versionId, name } = selectRow[0];
      const { code, data } = await getTaskInfo(proId, { modelId, modelVersionId: versionId });
      if (code === 0) {
        const { backbone, framework, engine, tasks } = data;
        const tempUserParams = type === '1' ? tasks.train.userParams : tasks.eval.userParams;
        setTaskData({ ...data, userParams: tempUserParams, modelId });
        runningParamsRef.current.setRunningParams(tempUserParams);
        setFieldsValue({
          backbone,
          framework,
          engine,
          modelName: name,
          visualization: tasks.train.visualization,
          userParams: tempUserParams.map(i => { return { ...i, value: i.default } })
        });
        onCloseModal();
      }
    } else {
      message.warning('请选择一个模型！');
    }
  };

  const onExpandedRowsChange = (r) => {
    const rows = r[r.length - 1];
    setExpandedRows(rows ? [rows] : []);
    setSelectRow([]);
  };

  const getTypeText = () => {
    if (Array.isArray(taskData.devices) && taskData.devices.length) {
      const obj = {};
      taskData.devices.forEach((i) => {
        const { type, series, deviceNum, cpu, memory, model } = i;
        const text = `所选模型资源要求：${type}${` ${series}` || ` ${model}` || ''}:${deviceNum || '-'}卡 ${cpu || '-'}核${memory || '-'}G`;
        if (type.includes('cpu')) obj.CPU = text;
        if (type.includes('npu')) obj.NPU = text;
        if (type.includes('gpu')) obj.GPU = text;
      });
      return obj;
    }
  };

  const text3 = formatMessage({ id: 'Cancel', defaultMessage: '取消' });
  const text4 = formatMessage({ id: 'Confirm', defaultMessage: '确定' });

  if (pageLoading) return <PageLoading />;

  return (
    <div className={styles.trainWrap}>
      <PageContainer title={type === '1' ? <SelectProTitle getData={getData} name={proData.name} allProData={allProData} /> : null}>
        <PageHeader
          ghost={false}
          title={`创建模型${typeText}`}
          onBack={() =>
            history.push(`/project/modelDevelop?proId=${proId}&isTable=${type === '2' ? 1 : 0}&labId=${labId}`)
          }
        />
        <Card>
          {lineTitle(`${typeText}配置`)}
          <Form form={form} initialValues={{ useNNI }}>
            {type === '1' ? (
              <>
                <Form.Item label="模型名称" name="modelName" rules={[{ required: true }]}>
                  <Form.Item name="modelName" className={styles.speItem}>
                    <Input disabled placeholder="请点击浏览按钮选择模型" />
                  </Form.Item>
                  <div className={styles.btnWrap}>
                    <Button type="primary" onClick={() => setSelectModalFlag(true)}>
                      浏览
                    </Button>
                    <EllipsisTooltip isQuestion text="模型来源于模型工厂APWorkshop" />
                  </div>
                </Form.Item>
                <Form.Item name="framework" label="模型框架" rules={[{ required: true }]}>
                  <Input disabled />
                </Form.Item>
                <Form.Item name="backbone" label="网络结构">
                  <Input disabled />
                </Form.Item>
                <Form.Item name="engine" label="容器镜像" rules={[{ required: true }]}>
                  <Input disabled />
                </Form.Item>
                <Form.Item name="visualization" label="训练可视化">
                  <Input disabled />
                </Form.Item>
              </>
            ) : (
              <div className={styles.evalTextWrap}>
                <p>训练任务：{runId}&nbsp;&nbsp;&nbsp;{`#${taskData.num}`}</p>
                <p>训练模型：{taskData.modelName}&nbsp;&nbsp;&nbsp;{taskData.modelVersionName} </p>
              </div>
            )}
            {lineTitle('超参配置')}
            {type === '1' && (
              <Form.Item name="useNNI" label="参数类型">
                <Radio.Group onChange={(e) => setUseNNI(e.target.value)}>
                  <Radio value={false}>人工调参</Radio>
                  <Radio value disabled={!(taskData && taskData.tasks && taskData.tasks.nni)}>
                    自动调参(NNI)
                    <EllipsisTooltip
                      isQuestion
                      text="自动调参除了引擎镜像需要集成NNI环境外，还需要代码支持"
                    />
                  </Radio>
                </Radio.Group>
              </Form.Item>
            )}
            {((type === '1' && !useNNI) || type === '2') && getParamsItem()}
            {lineTitle('数据配置')}
            <Form.Item name="dataSets" label="数据集" rules={[{ required: true }]} className="speSelectItem">
              <Select showSearch filterOption={SelectFilter} placeholder="请选择数据集" allowClear>
                {datasetArr.map((i) => (
                  <Option value={JSON.stringify(i)}><EllipsisTooltip text={`${i.name}-${i.version}`} /></Option>
                ))}
              </Select>
            </Form.Item>
            {lineTitle('训练资源')}
            <ResourceForm
              form={form}
              isPreset
              taskData={taskData}
              typeText={getTypeText()}
              isEval={type === '2'}
            />
            {/* {type === '1' && getCheckBox('b', '本地高速缓存（SSD）', '加速训练执行速度')}
            {type === '1' && getCheckBox('c', '采用Spark集群', '加速训练执行速度')} */}
          </Form>
          <Button
            style={{ marginTop: 16 }}
            loading={btnLoading}
            type="primary"
            onClick={onCreatTask}
          >
            提交
          </Button>
        </Card>
        {selectModalFlag && (
          <Modal
            title="选择预置模型或发布模型"
            visible={selectModalFlag}
            onCancel={onCloseModal}
            destroyOnClose
            maskClosable={false}
            width="80%"
            className="selectModalWrap"
            footer={[
              <Button onClick={onCloseModal}>{text3}</Button>,
              <Button type="primary" onClick={onSelectModel}>
                {text4}
              </Button>,
            ]}
          >
            <ATable
              dataSourceAPIPath={`/studioModels?field=${proData.field}&task=${proData.task}`}
              columns={columns}
              request={request}
              reloadIcon={false}
              scroll={{ x: 1280 }}
              rowKey="id"
              sticky
              expandable={{
                rowExpandable: () => true,
                expandedRowRender,
                onExpandedRowsChange: onExpandedRowsChange,
                expandedRowKeys: expandedRows,
              }}
              useProForm
              searchToolBar={[
                {
                  type: 'select',
                  name: 'source',
                  label: '来源',
                  formItemProps: {
                    options: [{ label: '预置', value: 'preset' }, { label: '发布', value: 'publish' }],
                    placeholder: '请选择来源',
                    allowClear: true,
                  },
                },
                {
                  type: 'input',
                  name: 'name',
                  label: '模型名称',
                  formItemProps: {
                    placeholder: "请输入模型名称进行搜索",
                  },
                },
              ]}
            />
          </Modal>
        )}
      </PageContainer>
    </div>
  );
};

export default Train;
