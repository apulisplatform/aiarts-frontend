import React, { useState } from 'react';
import { Descriptions, Button, Input, message } from 'antd';
import { StatusTag } from '@/components';
import { editDesc } from '../service';

const widerSpanArr = ['description'];
const { TextArea } = Input;

const DetailItem = ({ proId, labId, runId, itemArr, callBack, setDescdata, descData, detailData }) => {
  const [isEdit, setIsEdit] = useState(false);

  const onEditDesc = async () => {
    if (isEdit) {
      const { code } = await editDesc(proId, labId, runId, { description: descData });
      if (code === 0) {
        message.success('修改成功！');
        callBack && callBack();
        setIsEdit(false);
      }
    } else {
      setIsEdit(true);
    }
  }

  const formatParmas = (p) => {
    let params = '';
    p &&
      p.length &&
      p.forEach((i) => {
        params += `${i.name}=${i.value}; `;
      });
    return params;
  };

  const getValContent = (key, diyValue) => {
    if (key === 'description') {
      return (
        <div style={{ position: 'relative' }}>
          <div style={{ width: '90%', minHeight: 54 }}>
            {isEdit ? 
            <TextArea value={descData} onChange={e => setDescdata(e.target.value)}  /> :
            <div>{descData}</div>}
          </div>
          <Button type="primary" style={{ position: 'absolute', right: 0, top: 14 }} onClick={onEditDesc}>{isEdit ? '提交' : '编辑'}</Button>
        </div>
      )
    } 
    if (key === 'userParams') {
      return formatParmas(diyValue);
    }
    return diyValue || (key === 'status' ? <StatusTag runId={runId} status={detailData[key]} /> : detailData[key]);
  }

  const content = itemArr.map((i) => {
    const { key, label, diyValue } = i;
    return (
      <Descriptions.Item label={label} key={key} span={widerSpanArr.includes(key) ? 3 : 1}>
        {getValContent(key, diyValue)}
      </Descriptions.Item>
    );
  });

  return <Descriptions bordered size="middle">{content}</Descriptions>
}

export default DetailItem;