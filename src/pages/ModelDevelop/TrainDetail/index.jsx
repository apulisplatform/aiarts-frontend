import React, { useEffect, useState } from 'react';
import { Card, Button, Spin, Pagination, Empty, Collapse } from 'antd';
import { PageLoading } from '@ant-design/pro-layout';
import { CaretRightOutlined } from '@ant-design/icons';
import { getPageQuery, getMemoryNum } from '@/utils/utils';
import { fetchLogs, getTaskDetail } from '../service';
import styles from './index.less';
import DetailItem from '../DetailItem';

const { Panel } = Collapse;

const TaskDetail = (props) => {
  const { runId, download = true, onActionTrain, GoToTrain, isSelf } = props;
  const [logs, setLogs] = useState([]);
  const [logLoading, setlogLoading] = useState(false);
  const [loading, setLoading] = useState(true);
  const [detailData, setDetailData] = useState(null);
  const [total, setTotal] = useState(1);
  const [descData, setDescdata] = useState(null);
  const [current, setCurrent] = useState(1);
  const [activeKey, setActiveKey] = useState(['1']);
  const { proId, labId } = getPageQuery();

  const getData = async () => {
    setLoading(true);
    const { code, data } = await getTaskDetail(proId, labId, runId);
    if (code === 0) {
      setDetailData(data);
      setDescdata(data.description);
      setLoading(false);
    }
  };

  const getLogs = async () => {
    setlogLoading(true);
    const { code, data } = await fetchLogs(proId, labId, runId, { pageNum: current });
    if (code === 0) {
      const { items, hasNext } = data;
      setLogs(items)
      if (hasNext && !(current < total)) {
        setTotal(current + 1);
      }
    }
    setlogLoading(false);
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    getLogs()
  }, [current]);

  const getResourceCard = () => {
    if (detailData?.quota?.request) {
      const { node, request } = detailData.quota;
      const { cpu, memory, device } = request;
      const { computeType, deviceNum, deviceType, series } = device;
      return (
        <Card style={{ marginTop: '24px' }}>
          资源规格={computeType === 'CPU' ? computeType : `${deviceType}-${series}`}-{cpu}核{getMemoryNum(memory)}
          ，设备数量={deviceNum}，调度节点数量={node || 1}
        </Card>
      );
    }
    return null;
  };

  const getItemArr = () => {
    const { tags, config } = detailData;
    const { datasetName, datasetVersion, engine, modelName, visualization, modelVersionName } = tags;
    const itemArr = [
      { key: 'runId', label: '任务ID' },
      { key: 'modelName', label: '训练模型', diyValue: `${modelName}: ${modelVersionName}` },
      { key: 'datasetname', label: '数据集', diyValue: `${datasetName}: ${datasetVersion}` },
      { key: 'engine', label: '训练镜像', diyValue: engine },
      { key: 'visualization', label: '训练可视化', diyValue: visualization },
      { key: 'userParams', label: '运行参数', diyValue: config.train.userParams },
      { key: 'description', label: '训练备注' },
    ];

    return (
      <DetailItem 
        itemArr={itemArr} 
        proId={proId} 
        labId={labId} 
        runId={detailData.runId} 
        callBack={() => getData()}
        setDescdata={setDescdata}
        descData={descData}
        detailData={detailData}
      />
    )
  };

  const onCollapseChange = (k) => {
    if (k.includes('4')) getLogs();
    setActiveKey(k);
  };

  const onGetLogs = () => {
    if (!activeKey.includes('4')) {
      const keys = activeKey;
      keys.push('4');
      setActiveKey(keys);
    }
    getLogs({ pageNum: 1 });
  };

  if (loading) return <PageLoading />;

  const hasActionBtn = (status) => {
    return (
      (status > 99 && isSelf) || (status < 100 && status !== 5 && status !== 6 && status !== 99)
    );
  };

  const onDownload = e => {
    e.stopPropagation();
    window.open(
      `/ai-arts/api/v1/projects/${proId}/labs/${labId}/runs/${runId}/fetch-logs?token=${localStorage.token}`,
    );
  }

  const onGetLogClick = e => {
    e.stopPropagation();
    onGetLogs();
  }

  return (
    <div className={styles.taskDetailWrap}>
      <Collapse
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        defaultActiveKey={['1']}
        activeKey={activeKey}
        onChange={onCollapseChange}
      >
        <Panel
          header="详情信息"
          key="1"
          extra={
            <>
              {hasActionBtn(detailData.status) && (
                <Button type="primary" danger onClick={() => onActionTrain(runId, 'kill')}>
                  {detailData.status > 99 ? '删除' : '停止'}
                </Button>
              )}
              <Button
                type="primary "
                style={{ margin: '0 16px' }}
                onClick={() => GoToTrain(runId, 1)}
              >
                复制
              </Button>
              <Button
                type="primary"
                disabled={detailData.status !== 100}
                onClick={() => GoToTrain(runId, 2)}
              >
                评估
              </Button>
            </>
          }
        >
          {getItemArr()}
        </Panel>
        {/* <Panel header="Tensorboard 1.1/Mindinsight 1.0（训练可视化）" key="2">
          Tensorboard 1.1/Mindinsight 1.0（训练可视化）
        </Panel>
        <Panel header="NNI 1.1（自动调参）" key="3">
          NNI 1.1（自动调参）
        </Panel> */}
        <Panel
          header="日志信息"
          key="4"
          extra={
            <>
              <Button type="primary" onClick={(e) => onGetLogClick(e)}>
                获取日志
              </Button>
              {download && logs && logs.length > 0 && (
                <Button
                  type="primary"
                  style={{ marginLeft: 16 }}
                  onClick={(e) => onDownload(e)}
                >
                  下载
                </Button>
              )}
            </>
          }
        >
          <Spin spinning={logLoading}>
            {logs && logs.length
              ? <pre style={{ height: 800, background: '#eee' }}>{logs}</pre>
              : <Empty description='暂无日志' />
            }
          </Spin>
          {total ?
            <Pagination
              pageSize={1}
              total={total}
              current={current}
              style={{ textAlign: 'right', margin: '24px 0' }}
              onChange={p => setCurrent(p)}
              showSizeChanger={false}
            /> : null
          }
        </Panel>
      </Collapse>
      {getResourceCard()}
    </div>
  );
};

export default TaskDetail;
