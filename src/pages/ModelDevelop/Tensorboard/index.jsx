import React from 'react';
import { Space, message } from 'antd';
import startIcon from '@/assets/visualJob_start.svg';
import stopIcon from '@/assets/visualJob_stop.svg';
import entryIcon from '@/assets/visualJob_entry.svg';
import runningIcon from '@/assets/visualJob_running.svg';
import notRunningIcon from '@/assets/visualJob_notRunning.svg';
import { GetJobStatus } from '@/utils/status';
import { switchVisualizationJobStatus } from './services';

const Tensorboard = (props) => {
  const { runInfo, getData, setPageLoading } = props;
  const { runId, status, labId, viewStatus, proId, tags } = runInfo;
  const canShowStatus = [7, 99, 100, 101, 102, 104];

  const handleAction = async (action, isEntry) => {
    setPageLoading && setPageLoading(true);
    const { code, data } = await switchVisualizationJobStatus(
      proId,
      labId,
      runId,
      action,
      tags.visualization,
    );
    if (code === 0) {
      if (isEntry) {
        window.open(data, '_blank');
      } else {
        message.success(`${action === 'open_visual' ? '启动' : '暂停'}成功！`);
        getData && getData();
      }
    } else if (code === 280000023) {
      message.success('正在启动中,请稍后');
    }
    setPageLoading && setPageLoading(false);
  };

  return (
    <div style={{ display: 'flex', alignItems: 'center', height: '60%' }}>
      <Space>
        {/* <Space> */}
          <div>
            <img
              src={viewStatus === 7 ? runningIcon : notRunningIcon}
              alt={GetJobStatus(viewStatus)}
            />
            <span>&nbsp;{GetJobStatus(viewStatus)}</span>
          </div>
        {/* </Space> */}

        {canShowStatus.includes(status) && (
          <Space>
            {viewStatus > 0 && viewStatus < 100 && ![5, 6, 99].includes(viewStatus) ? (
              <>
                |
                <div onClick={() => handleAction('close_visual')} style={{ cursor: 'pointer' }}>
                  <img src={stopIcon} alt="停止" />
                </div>
              </>
            ) : (
              <>
                {(viewStatus >= 100 || !viewStatus)  && (
                  <>
                    |
                    <div onClick={() => handleAction('open_visual')} style={{ cursor: 'pointer' }}>
                      <img src={startIcon} alt="开始" />
                    </div>
                  </>
                )}
              </>
            )}
            {viewStatus === 7 && (
              <div onClick={() => handleAction('open_visual', true)} style={{ cursor: 'pointer' }}>
                <img src={entryIcon} alt="进入" />
              </div>
            )}
          </Space>
        )}
      </Space>
    </div>
  );
};

export default Tensorboard;
