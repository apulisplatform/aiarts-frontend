import request from '@/utils/request';

export async function switchVisualizationJobStatus(proId, labId, runId, action, visualization) {
  return await request(`/projects/${proId}/labs/${labId}/runs/${runId}`, {
    method: 'POST',
    params: {
      action, visualization
    },
  });
}