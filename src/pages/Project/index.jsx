import React, { useState, useRef, useEffect } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Modal, message } from 'antd';
import request from '@/utils/request';
import ATable from '@apulis/table';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Link, useIntl } from 'umi';
import { getPageQuery } from '@/utils/utils';
import EllipsisTooltip from '@/components/EllipsisTooltip';
import AddModalForm from './addModalForm';
import styles from './index.less';
import { createPro, deletePro, getStudioGetTypes } from './service';

const { confirm } = Modal;

const Project = () => {
  const { formatMessage } = useIntl();
  const [modalFlag, setModalFlag] = useState(getPageQuery().isOpen || false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [fieldType, setFieldType] = useState({});
  const [field, setField] = useState('');
  const tableRef = useRef();
  const addModalFormRef = useRef();

  const onDelete = (id, type) => {
    confirm({
      title: `确定删除该项目吗？${type ==='expert' ? `项目删除后，code和outputs目录将会被删除！` : ''}`,
      icon: <ExclamationCircleOutlined />,
      okText: formatMessage({ id: 'Delete', defaultMessage: '删除' }),
      okType: 'danger',
      cancelText: formatMessage({ id: 'Cancel', defaultMessage: '取消' }),
      centered: true,
      onOk: async () => {
        const { code } = await deletePro(id);
        if (code === 0) {
          tableRef.current.reload();
          message.success(formatMessage({ id: 'delete success！', defaultMessage: '删除成功！' }));
        }
      },
      onCancel() {},
    });
  };

  const columns = [
    {
      title: formatMessage({ id: 'Project Name', defaultMessage: '项目名称' }),
      dataIndex: 'name',
      render: (t, item) => (
        <Link
          title={t}
          to={
            item.type === 'preset'
              ? `/project/modelDevelop?proId=${item.id}&labId=${item.labId}`
              : `/project/expertDevelop?proId=${item.id}`
          }
        >
          {t}
        </Link>
      ),
    },
    {
      title: formatMessage({ id: 'Development Type', defaultMessage: '开发类型' }),
      dataIndex: 'type',
      render: (i) => <span>{i === 'preset' ? '预置模型开发' : '专家开发'}</span>,
    },
    {
      title: formatMessage({ id: 'Project Type', defaultMessage: '项目类型' }),
      dataIndex: 'field',
      render: (i, item) => <span>{item.type === 'preset' ? `${i}/${item.task}` : '--'}</span>,
    },
    {
      title: formatMessage({ id: 'Creator', defaultMessage: '创建人' }),
      dataIndex: 'creator',
    },
    {
      title: formatMessage({ id: 'Description', defaultMessage: '描述' }),
      dataIndex: 'description',
      ellipsis: true,
      render: (i) => <EllipsisTooltip text={i} />
    },
    {
      title: formatMessage({ id: 'Create Time', defaultMessage: '创建时间' }),
      key: 'createdAt',
      dataIndex: 'createdAt',
      format: 'time',
      sortable: true,
    },
    {
      title: formatMessage({ id: 'Action', defaultMessage: '操作' }),
      key: 'action',
      render: (t, item) => {
        return (
          <Button danger type="link" onClick={() => onDelete(item.id, item.type)}>
            {formatMessage({ id: 'Delete', defaultMessage: '删除' })}
          </Button>
        );
      },
    },
  ];

  const onCreatePro = () => {
    addModalFormRef.current.form.validateFields().then(async (values) => {
      setBtnLoading(true);
      const { code } = await createPro({ ...values, ...values.protype });
      if (code === 0) {
        message.success(formatMessage({ id: 'create success！', defaultMessage: '创建成功！' }));
        tableRef.current.reload();
      }
      setBtnLoading(false);
      setModalFlag(false);
    });
  };

  const getData = async () => {
    const { code, data } = await getStudioGetTypes();
    if (code === 0) setFieldType(data);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <PageContainer>
      <ATable
        dataSourceAPIPath="/projects/list"
        columns={columns}
        request={request}
        actionRef={tableRef}
        rowKey="id"
        sticky
        scroll={{ x: 1280 }}
        useProForm
        searchToolBar={[
          {
            type: 'select',
            name: 'field',
            label: '选择大类',
            formItemProps: {
              options: Object.keys(fieldType).map((i) => {
                return { label: i, value: i };
              }),
              placeholder: '请选择大类',
              allowClear: true,
            },
            selectProps: {
              onSelect: (v) => setField(v)
            }
          },
          {
            type: 'select',
            name: 'task',
            label: '选择小类',
            formItemProps: {
              placeholder: '请选择小类',
              options: field ? fieldType[field].task.map((i) => {
                return { label: i, value: i };
              }) : [],
              allowClear: true,
            },
            selectProps: {
              onFocus: () => { if (!field) message.warning('请先选择大类') }
            }
          },
          {
            type: 'select',
            name: 'type',
            label: '开发类型',
            formItemProps: {
              placeholder: '请选择开发类型',
              options: [
                { label: '专家模型', value: 'expert' },
                { label: '预置模型', value: 'preset' },
              ],
              allowClear: true,
            },
          },
          {
            type: 'input',
            name: 'name',
            label: '关键字',
            formItemProps: {
              placeholder: "请输入项目名称、创建人进行搜索",
            },
          },
        ]}
        leftToolBar={
          <Button type="primary" onClick={() => setModalFlag(true)}>
            {formatMessage({ id: 'New Project', defaultMessage: '新建项目' })}
          </Button>
        }
      />
      {modalFlag && (
        <Modal
          title={formatMessage({ id: 'New Project', defaultMessage: '新建项目' })}
          visible={modalFlag}
          onCancel={() => setModalFlag(false)}
          destroyOnClose
          maskClosable={false}
          className="addModalForm"
          width={600}
          footer={[
            <Button onClick={() => setModalFlag(false)}>
              {formatMessage({ id: 'Cancel', defaultMessage: '取消' })}
            </Button>,
            <Button type="primary" loading={btnLoading} onClick={onCreatePro}>
              {formatMessage({ id: 'Submit', defaultMessage: '提交' })}
            </Button>,
          ]}
        >
          <AddModalForm
            ref={addModalFormRef}
            setBtnLoading={setBtnLoading}
            fieldType={fieldType}
          />
        </Modal>
      )}
    </PageContainer>
  );
};

export default Project;
