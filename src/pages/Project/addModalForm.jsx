import React, { useImperativeHandle, forwardRef, useState } from 'react';
import { Form, Input, Card, Select, message } from 'antd';
import { useIntl } from 'umi';
import { SpeNameReg } from '@/utils/reg';
import icon1 from '@/assets/icon1.png';
import icon2 from '@/assets/icon2.png';
import styles from './index.less';

const { Meta } = Card;
const { Option } = Select;

const AddModalForm= ({ fieldType }, ref) => {
  const [form] = Form.useForm();
  const { setFieldsValue } = form;
  const { formatMessage } = useIntl();
  const [type, setType] = useState('expert');
  const [field, setField] = useState('');
  const textArr = [
    {
      title: '代码开发',
      desc: '提供灵活便捷的在线开发环境',
      icon: icon1,
      key: 'expert',
    },
    {
      title: '模型训练',
      desc: '提供简易便用的模型训练、评估和发布流程',
      icon: icon2,
      key: 'preset',
    },
  ];

  useImperativeHandle(ref, () => ({ form }));
  const formItemLayout = {
    labelCol: {
      span: 6,
    }
  };

  const onTypeChange = t => {
    if (t !== type) {
      setType(t);
      setFieldsValue({ type: t });
    }
  }

  const getCard = () => {
    return textArr.map((i, idx) => {
      const { title, desc, icon, key } = i;
      return (
        <Card
          hoverable
          cover={<img alt={title} src={icon} />}
          style={{ marginRight: `${idx ? 0 : '24px'}` }}
          onClick={() => onTypeChange(key)}
          className={key === type ? styles.actived : ''}
        >
          <Meta title={title} description={desc} />
        </Card>
      )
    })
  }

  return (
    <Form form={form} {...formItemLayout} initialValues={{ type }}>
      <Form.Item
        label={formatMessage({ id: 'Development Type', defaultMessage: '开发类型' })}
        name="type"
        rules={[{ required: true }]}
      >
        {getCard()}
      </Form.Item>
      {type === 'preset' && <Form.Item
        label={formatMessage({ id: 'Project Type', defaultMessage: '项目类型' })}
        name="protype"
        rules={[{ required: true, message: '' }]}
        className="speItem"
      >
        <Form.Item 
          name={['protype', 'field']}
          rules={[{ required: true, message: "请选择大类" }]}
        >
          <Select placeholder="请选择大类" onSelect={(v) => setField(v)}>
            {Object.keys(fieldType).map(i => <Option key={i} value={i}>{i}</Option>)}
          </Select>
        </Form.Item>
        <Form.Item 
          name={['protype', 'task']}
          rules={[{ required: true, message: "请选择小类" }]}
        >
          <Select placeholder="请选择小类" onFocus={() => { if (!field) message.warning('请先选择大类') }}>
            {field ? fieldType[field].task.map(i => <Option key={i} value={i}>{i}</Option>) : null}
          </Select>
        </Form.Item>
      </Form.Item>}
      <Form.Item
        label={formatMessage({ id: 'Project Name', defaultMessage: '项目名称' })}
        name="name"
        rules={[{ required: true, max: 20 }, {...SpeNameReg}]}
      >
        <Input placeholder={formatMessage({ id: 'please enter project name', defaultMessage: '请输入项目名称' })} />
      </Form.Item>
      <Form.Item
        label={formatMessage({ id: 'Project Description', defaultMessage: '项目描述' })}
        name="description"
        rules={[{ max: 200 }]} 
      >
        <Input.TextArea placeholder={formatMessage({ id: 'please enter a project description within 200 characters', defaultMessage: '请输入描述，限制200字符以内' })} autoSize={{ minRows: 4 }} />
      </Form.Item>
    </Form>
  );
};

export default forwardRef(AddModalForm);