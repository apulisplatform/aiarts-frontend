import request from '@/utils/request';

export async function getProjects(params) {
  return await request(`/projects/list`, {
    params
  });
}

export async function getProDetail(id) {
  return await request(`/projects/${id}`);
}

export async function createPro(data) {
  return await request(`/projects`, {
    method: 'POST',
    data: data
  });
}

export async function deletePro(id) {
  return request(`/projects/${id}`, {
    method: "DELETE"
  });
}

export async function getStudioGetTypes() {
  return await request(`/studioGetRelatedTypes`);
}