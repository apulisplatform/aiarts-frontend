import { useIntl } from 'umi';

export const GetJobStatus = (status) => {
  const { formatMessage } = useIntl();
  const statusList = {
    0: formatMessage({ id: 'unapproved', defaultMessage: '未启动' }),
    1: formatMessage({ id: 'initializing', defaultMessage: '初始化中' }),
    2: formatMessage({ id: 'starting', defaultMessage: '启动中' }),
    3: formatMessage({ id: 'queued', defaultMessage: '队列中' }),
    4: formatMessage({ id: 'scheduling', defaultMessage: '调度中' }),
    5: formatMessage({ id: 'killing', defaultMessage: '关闭中' }),
    6: formatMessage({ id: 'killing', defaultMessage: '等待关闭中' }),
    7: formatMessage({ id: 'running', defaultMessage: '运行中' }),
    99: formatMessage({ id: 'ending', defaultMessage: '结束中' }),
    100: formatMessage({ id: 'completed', defaultMessage: '已完成' }),
    101: formatMessage({ id: 'killed', defaultMessage: '已终止' }),
    102: formatMessage({ id: 'error', defaultMessage: '错误' }),
    103: formatMessage({ id: 'failed', defaultMessage: '失败' }),
    104: formatMessage({ id: 'save failed', defaultMessage: '保存失败' })
  };
  return statusList[status] || '未启动';
};
