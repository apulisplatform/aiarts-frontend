import requestUser from '@/utils/request-user';

export async function getPlatformConfig() {
  return await requestUser('/platform-config');
}