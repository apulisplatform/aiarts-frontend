import request from '@/utils/request';

export async function getGobTips(params) {
  return await request(`/misc/job-tips`, {
    params
  });
}
