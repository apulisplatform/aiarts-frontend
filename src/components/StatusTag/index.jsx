import React, { useState } from 'react';
import { Tag, Tooltip, Tabs, Spin } from 'antd';
import { getStatusColor } from '@/utils/utils';
import { GetJobStatus } from '@/utils/status';
import { QuestionCircleFilled } from '@ant-design/icons';
import { getGobTips } from './service';
import styles from './index.less';

const { TabPane } = Tabs;

const StatusTag = ({ status = 0, text, runId }) => {
  const arr = [4, 7, 102];

  const GetJobTipContent = () => {
    const [info, setInfo] = useState(null);
    const [loading, setLoading] = useState(false);

    const onVisibleChange = async (v) => {
      if (v) {
        setLoading(true);
        const { code, data } = await getGobTips({ jobId: runId });
        if (code === 0) setInfo(data);
        setLoading(false);
      }
    }

    const getPodItem = () => {
      const { jobId, podsInfo, jobInfo } = info;
      let obj = podsInfo || {};
      if (jobId) obj = { [jobId]: jobInfo, ...obj };
      return Object.keys(obj).map(i => {
        return (
          <TabPane tab={i} key={i}>
            <pre>{obj[i] || '暂无信息'}</pre>
          </TabPane>
        )
      }); 
    }

    const getContent = () => {
      if (loading) return <Spin />;
      if (info) {
        const { jobId, podsInfo } = info;

        if (jobId || podsInfo) {
          return (
            <Tabs type="card" className="statusTagTooltipWrap">
              <TabPane tab="调度信息" key="podsInfo">
                <Tabs>{getPodItem()}</Tabs>
              </TabPane>
              {/* <TabPane tab="日志信息" key="tips">
                <pre>{tips}</pre>
              </TabPane> */}
            </Tabs>
          )
        }
      }
      return <pre />;
    }

    return (
      <Tooltip
        placement="right"
        overlayStyle={{ maxWidth: 1000, maxHeight: 1000, overflow: 'scroll' }}
        title={getContent()}
        trigger="click"
        onVisibleChange={onVisibleChange}
      >
        <QuestionCircleFilled />
      </Tooltip>
    )
  }

  return (
    <span className={styles.statusTagWrap}>
      <Tag color={getStatusColor(status)}>{text || GetJobStatus(status)}</Tag>
      {arr.includes(status) && runId && GetJobTipContent()}
    </span>
  )
}

export default StatusTag;