/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useImperativeHandle, forwardRef } from 'react';
import { Form, Input, InputNumber } from 'antd';
import { DeleteOutlined, PlusSquareOutlined, PauseOutlined } from '@ant-design/icons';
import { generateKey } from '@/utils/utils';
import styles from './index.less';

const RuningParamsItem = ({ form, data, canChangeArr = false, canChangeKey = false }, ref) => {
  const [runningParams, setRunningParams] = useState(data || []);
  const { getFieldValue, setFieldsValue } = form;
  
  useImperativeHandle(ref, () => ({
    runningParams: runningParams,
    setRunningParams: setRunningParams
  }));

  const addParams = () => {
    const newRunningParams = runningParams.concat({
      name: '',
      default: '',
      createTime: generateKey(),
    });
    setRunningParams(newRunningParams);
  };

  const removeRuningParams = async (name) => {
    const values = await getFieldValue('params');
    if (values.length === 1) {
      setFieldsValue({
        params: [{ name: '', default: '' }],
      });
    } else {
      [...runningParams].forEach((p, index) => {
        p.name = values[index].name;
        p.default = values[index].default;
      });
      const newRunningParams = [...runningParams].filter(p => {
        if (p.createTime) {
          return p.createTime !== name;
        }
        return p.name !== name;
      });
      setRunningParams(newRunningParams);
      setFieldsValue({
        params: newRunningParams.map(p => ({ name: p.name, default: p.default })),
      });
    }
  };

  const getContent = () => {
    if (runningParams && runningParams.length) {
      return runningParams.map((param, index) => {
        const { name, createTime, editable, type, required, value } = param;
        const val = value || param.default;
        const typeTemp = type || 'string';
        return (
          <div key={index}>
            <Form.Item
              initialValue={name}
              name={['userParams', index, 'name']}
            >
              <Input disabled={!canChangeKey} />
            </Form.Item>
            <PauseOutlined rotate={90} />
            <Form.Item
              initialValue={typeTemp === 'string' ? val : Number(val)}
              name={['userParams', index, 'value']}
              rules={[{ required: required, message: '请输入参数值' }]}
            >
              {typeTemp === 'string' ? <Input disabled={!editable} /> : <InputNumber disabled={!editable} />}
            </Form.Item>
            {canChangeArr && runningParams.length > 1 &&
            <DeleteOutlined
              onClick={() => removeRuningParams(createTime || name)}
            />}
          </div>
        );
      });
    }
    return null;
  }

  return (
    <div className={styles.runingParamsItemWrap}>
      {getContent()}
      {canChangeArr && <span onClick={addParams}>
        <PlusSquareOutlined fill="#4168AA" />
        <a>点击增加参数</a>
      </span>}
    </div>
  )
}

export default forwardRef(RuningParamsItem);
