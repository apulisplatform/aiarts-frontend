import React from 'react';
import { CopyOutlined } from '@ant-design/icons';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { message } from 'antd';
import styles from './index.less';

const TextCopyToClipboard = ({ text }) => {
  return (
    <span className={styles.textCopyToClipboardWrap}>{text}
      <CopyToClipboard
        text={text}
        onCopy={() => message.success('复制成功！')}
      >
        <CopyOutlined />
      </CopyToClipboard>
    </span>
  )
}

export default TextCopyToClipboard;