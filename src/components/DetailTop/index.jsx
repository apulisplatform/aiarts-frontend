/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { EllipsisTooltip } from '@/components';
import {
  UserOutlined,
  TagOutlined,
  FieldTimeOutlined,
  EditOutlined,
  CaretRightOutlined,
  FireOutlined,
  CodepenOutlined,
  DeploymentUnitOutlined,
  IdcardOutlined,
  FileTextOutlined,
} from '@ant-design/icons';
import { useIntl } from 'umi';
import { Card } from 'antd';
import moment from 'moment';
import styles from './index.less';

const DetailTop = ({ data, proId, step, setStep, ismodelDevelop = false }, ref) => {
  const { formatMessage } = useIntl();
  const getTagInfo = () => {
    if (data) {
      const arr = [
        {
          tag: <IdcardOutlined />,
          val: `${formatMessage({ id: 'Project', defaultMessage: '项目' })} ID：${proId}`,
        },
        {
          tag: <TagOutlined />,
          val:
            data.type === 'preset'
              ? formatMessage({ id: 'Model Develop', defaultMessage: '预置模型开发' })
              : formatMessage({ id: 'Expert Develop', defaultMessage: '专家开发' }),
        },
        {
          tag: <UserOutlined />,
          val: data.creator || 'asdfasdfsdaf',
        },
        {
          tag: <FieldTimeOutlined />,
          val: moment(data.createdAt).format('YYYY-MM-DD HH:mm:ss'),
        },
      ];
      return arr.map((i) => (
        <span className={styles.tagInfo}>
          {i.tag}
          {i.val}
        </span>
      ));
    }
    return null;
  };

  const getIconSteps = () => {
    const onStepClick = (idx, url) => {
      if (ismodelDevelop && idx === 0) return;
      if (idx + 1 === step) return;
      if (idx < 2) setStep(idx + 1);
      if (url) window.open(url, '_blank');
    };
    const HOST = window.location.origin;

    const arr = [
      {
        text: formatMessage({ id: 'Code Develop', defaultMessage: '代码开发' }),
        icon: <EditOutlined />,
        onClick: onStepClick,
      },
      {
        text: formatMessage({ id: 'Model Training', defaultMessage: '模型训练' }),
        icon: <FireOutlined />,
        onClick: onStepClick,
      },
      {
        text: formatMessage({ id: 'Model Factory', defaultMessage: '模型工厂' }),
        icon: <CodepenOutlined />,
        onClick: (idx) => onStepClick(idx, `${HOST}/AIStudio/apworkshop/models`),
      },
      {
        text: formatMessage({ id: 'Inference Center', defaultMessage: '推理中心' }),
        icon: <DeploymentUnitOutlined />,
        onClick: (idx) => onStepClick(idx, `${HOST}/AIStudio/apflow`),
      },
    ];
    return arr.map((i, idx) => {
      const isDisable = ismodelDevelop && step === 2 && idx === 0;
      return (
        <div className={styles.iconStepsWrap}>
          <div
            className={`${idx + 1 === step ? styles.checked : ''} ${styles.stepWrap} ${isDisable ? styles.disable : ''}`}
            onClick={() => !isDisable && i.onClick(idx)}
          >
            {i.icon}
            <div>
              <EllipsisTooltip text={i.text} />
            </div>
          </div>
          {idx !== arr.length - 1 && (
            <CaretRightOutlined style={{ margin: '0 10px', paddingTop: 25, verticalAlign: 'top' }} />
          )}
        </div>
      )
    });
  };

  return (
    <Card className={styles.detailTopWrap}>
      <div className={styles.halfWrap}>
        {getTagInfo()}
        <div className={styles.desc}>
          <FileTextOutlined />
          <span style={{ verticalAlign: 'super' }}>
            {formatMessage({ id: 'description', defaultMessage: '描述' })}：
          </span>
          <span style={{ width: '80%', display: 'inline-block' }}>
            <EllipsisTooltip text={data.description || '--'} />
          </span>
        </div>
      </div>
      <div className={styles.halfWrap} style={{ textAlign: 'right' }}>
        {getIconSteps()}
      </div>
    </Card>
  );
};

export default DetailTop;
