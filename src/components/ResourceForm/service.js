import request from '@/utils/request';

export async function getResourceQuotas(params) {
  return await request(`/resource-quotas`, { params });
}

export async function getQuotaLimits(params) {
  return await request(`/quota-limits`, { params });
}