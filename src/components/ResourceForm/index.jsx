/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useEffect, forwardRef } from 'react';
import { Form, Select, Radio, message } from 'antd';
import { getPageQuery, getMemoryNum } from '@/utils/utils';
import { useIntl } from 'umi';
import { EllipsisTooltip } from '@/components';
import styles from './index.less';
import { getResourceQuotas, getQuotaLimits } from './service';

const { Option } = Select;

const ResourceForm = (props) => {
  const { formatMessage } = useIntl();
  const { form, resourceTypeData, isPreset, taskData, typeText, isEval } = props;
  const [resourceName, setResourceName] = useState(null);
  const [resourceNumber, setResourceNumber] = useState(0);
  const [resourceType, setResourceType] = useState(resourceTypeData || (isPreset ? null : 'CPU'));
  const [quotaOptions, setQuotaOptions] = useState({});
  const [resourceOptions, setResourceOptions] = useState({});
  const isCPU = resourceType === 'CPU';
  const text1 = formatMessage({
    id: 'please select single card resource specification',
    defaultMessage: '请选择资源规格',
  });
  const text2 = formatMessage({ id: 'please select card number', defaultMessage: '请选择卡数' });
  const text3 = formatMessage({
    id: 'please select node number',
    defaultMessage: '请选择节点数量',
  });
  const { setFieldsValue, getFieldValue } = form;

  const getResourceType = async () => {
    const obj = { type: resourceType };
    if (
      !isPreset ||
      (isEval && resourceType) ||
      (!isEval && isPreset && taskData.modelId && resourceType)
    ) {
      if (isEval || isPreset) {
        obj.modelId = taskData.modelId;
        obj.modelVersionId = taskData.versionId;
      }
      const { code, data } = await getResourceQuotas(obj);
      if (code === 0) {
        setQuotaOptions({
          ...quotaOptions,
          [resourceType]: data,
        });
      }
    }
  };

  useEffect(() => {
    setFieldsValue({ resource: null, nodeNumber: null, resourceType: resourceType });
    setResourceOptions({});
    setQuotaOptions({});
    setResourceName(null);
    setResourceNumber(0);
    getResourceType();
  }, [resourceType, taskData]);

  useEffect(() => {
    if (typeText && Object.keys(typeText).length) {
      setResourceType(Object.keys(typeText)[0]);
    }
  }, [typeText]);

  const onResourceNameChange = async (v) => {
    const obj = {
      quotaId: JSON.parse(v).id,
    };
    if (isEval || isPreset) {
      obj.modelId = taskData.modelId;
      obj.modelVersionId = taskData.versionId;
    }
    const { code, data } = await getQuotaLimits(obj);
    if (code === 0) {
      const temp = {};
      data.forEach((i) => {
        temp[i.num] = i.node;
      });
      setResourceOptions(temp);
    } else {
      setResourceOptions({});
    }
    setFieldsValue({
      resource: { resourceName: v, resourceNumber: null },
      nodeNumber: null,
    });
    setResourceNumber(0);
    setResourceName(v);
  };

  const getResourceTypeOptions = () => {
    if (quotaOptions && quotaOptions[resourceType] && quotaOptions[resourceType].length) {
      return quotaOptions[resourceType].map((i) => {
        const { arch, cpuNum, mem, model, series } = i;
        let tempText = arch;
        const text = `${cpuNum}核${getMemoryNum(mem)}`;
        if (!isCPU) tempText = `${model}-${series}`;
        return (
          <Option key={JSON.stringify(i)} value={JSON.stringify(i)}>
            <EllipsisTooltip text={`${tempText}-${text}`} />
          </Option>
        );
      });
    }
  };

  const getResourceNumberOption = () => {
    if (resourceName && !isCPU && Object.keys(resourceOptions).length) {
      return Object.keys(resourceOptions).map((i) => (
        <Option key={Number(i)} value={Number(i)}>
          <EllipsisTooltip text={Number(i)} />
        </Option>
      ));
    }
    return null;
  };

  const getNodeNumberOption = () => {
    if (!isCPU && !resourceNumber) return null;
    if (resourceName && Object.keys(resourceOptions).length) {
      const key = isCPU ? 0 : resourceNumber;
      if (resourceOptions[key]) {
        const data = Array.from(new Array(resourceOptions[key] + 1).keys()).slice(1);
        return data.map((i) => (
          <Option key={i} value={i}>
            <EllipsisTooltip text={i} />
          </Option>
        ));
      }
      return null;
    }
    return null;
  };

  const onResourceTypeChange = (e) => {
    if (!isEval && isPreset && !taskData.modelId) {
      message.warning('请先选择一个模型');
      setFieldsValue({ resourceType: null });
    } else {
      setResourceType(e.target.value);
      setFieldsValue({ resourceType: e.target.value });
    }
  };

  const getResourceInfo = () => {
    let manifesText = '';
    const noText = '暂无满足条件的资源';
    const { modelId, devices } = taskData;
    if (modelId) {
      if (Array.isArray(devices) && devices.length) {
        manifesText = '所选模型资源要求为：';
        devices.forEach((i, idx) => {
          const { cpu, deviceNum, memory, series, type } = i;
          manifesText += `${type}${series ? `-${series}` : ''}：${deviceNum}卡，cpu：${cpu}核，memory：${getMemoryNum(memory)}；${idx === devices.length - 1 ? '' : <br />}`;
        });
      }
      if (quotaOptions && quotaOptions[resourceType] && quotaOptions[resourceType].length) {
        return manifesText;
      }
      return `${manifesText === '' ? '' : `${manifesText}；`}${noText}`;
    }
    return '请先选择一个模型！';
  }

  return (
    <div className={styles.resourceFormWrap}>
      <div className={isPreset ? styles.typeWrap : null}>
        <Form.Item
          name="resourceType"
          label={formatMessage({ id: 'resource type', defaultMessage: '资源类型' })}
          rules={[{ required: true, message: '请选择资源类型' }]}
        >
          <Radio.Group
            options={[
              {
                label: 'CPU',
                value: 'CPU',
                disabled: typeText ? !Object.keys(typeText).includes('CPU') : false,
              },
              {
                label: 'GPU',
                value: 'GPU',
                disabled: typeText ? !Object.keys(typeText).includes('GPU') : false,
              },
              {
                label: 'NPU',
                value: 'NPU',
                disabled: typeText ? !Object.keys(typeText).includes('NPU') : false,
              },
            ]}
            onChange={onResourceTypeChange}
            optionType="button"
          />
        </Form.Item>
      </div>
      <div className={styles.resourceWrap}>
        <Form.Item
          name="resource"
          label={formatMessage({ id: 'resource use', defaultMessage: '资源使用' })}
          rules={[{ required: true, message: '' }]}
          className="speItemWrap"
        >
          <Form.Item
            rules={[{ required: true, message: text1 }]}
            name={['resource', 'resourceName']}
            className="speItem speSelectItem"
          >
            <Select
              onFocus={() => {
                if (!resourceType) message.warning('请先选择资源类型');
              }}
              onChange={onResourceNameChange}
              placeholder={text1}
            >
              {getResourceTypeOptions()}
            </Select>
          </Form.Item>
          <Form.Item
            rules={[{ required: !isCPU, message: text2 }]}
            name={['resource', 'resourceNumber']}
            className="speItem2 speSelectItem"
          >
            <Select
              onFocus={() => {
                if (!getFieldValue('resource')) message.warning(text1);
              }}
              onChange={(v) => setResourceNumber(v)}
              placeholder={text2}
              disabled={resourceType === 'CPU'}
            >
              {getResourceNumberOption()}
            </Select>
          </Form.Item>
        </Form.Item>
        {isPreset && <EllipsisTooltip isQuestion text={getResourceInfo()} />}
      </div>
      {!isEval && (
        <div className={styles.numWrap}>
          <Form.Item
            name="nodeNumber"
            label={<>{formatMessage({ id: 'node number', defaultMessage: '节点数量' })}</>}
            rules={[{ required: true, message: text3 }]}
            className="speSelectItem"
          >
            <Select
              onFocus={() => {
                if (!resourceNumber && !isCPU) message.warning(text2);
              }}
              placeholder={text3}
            >
              {getNodeNumberOption()}
            </Select>
          </Form.Item>
          <EllipsisTooltip
            isQuestion
            text={formatMessage({
              id: 'When the number of nodes exceeds one, the resource will be scheduled on multiple nodes, and the training tasks will become distributed tasks',
              defaultMessage:
                '当节点数量超过1个，资源将会在多个节点上进行调度，训练任务将变成分布式任务',
            })}
          />
        </div>
      )}
    </div>
  );
};

export default forwardRef(ResourceForm);
