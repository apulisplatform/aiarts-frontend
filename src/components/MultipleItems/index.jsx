import React, { useState, useImperativeHandle, forwardRef } from 'react';
import { Form, Select } from 'antd';
import { DeleteOutlined, PlusSquareOutlined } from '@ant-design/icons';
import EllipsisTooltip from '@/components/EllipsisTooltip';
import { generateKey } from '@/utils/utils';
import styles from './index.less';

const { Option } = Select;

const MultipleItem = ({ form, data, typeText, propKey, max = 999, options }, ref) => {

  const getObj = () => {
    const obj = { key: '', createTime: generateKey() };
    return obj;
  }

  const [multipleItems, setMultipleItems] = useState(data && data.length ? data : [getObj()]);
  const { getFieldValue, setFieldsValue } = form;

  useImperativeHandle(ref, () => ({
    multipleItems: multipleItems
  }));

  const addParams = () => {
    const newMultipleItems = multipleItems.concat(getObj());
    setMultipleItems(newMultipleItems);
  };

  const RemoveMultipleItems = async (v) => {
    const values = await getFieldValue(propKey);
    [...multipleItems].forEach((p, index) => {
      p.key = values[index].key;
    });
    const newMultipleItems = [...multipleItems].filter(p => {
      if (p.createTime) {
        return p.createTime !== v;
      } 
      return p.key !== v;
    });
    setMultipleItems(newMultipleItems);
    setFieldsValue({
      [propKey]: newMultipleItems.map(p => {
        const obj = { key: p.key };
        return obj;
      }),
    });
  };

  const SelectFilter = (input, option) => {
    return option.children.props.text.toLowerCase().indexOf(input.toLowerCase()) >= 0;
  }

  const getContent = () => {
    if (multipleItems && multipleItems.length) {
      return multipleItems.map((param, index) => {
        const { createTime, key } = param;
          return (
            <div key={index}>
              <Form.Item
                initialValue={multipleItems[index].key || null}
                name={[propKey, index, 'key']}
                style={{ width: '88%' }}
                className="speSelectItem"
              >
                <Select showSearch filterOption={SelectFilter} placeholder="请选择数据集" allowClear>
                  {Array.isArray(options) ? options.map(i => <Option value={JSON.stringify(i)} key={JSON.stringify(i)}><EllipsisTooltip text={`${i.name}-${i.version}`} /></Option>) : null}
                </Select>
              </Form.Item>
              {multipleItems.length > 1 && 
                <DeleteOutlined
                  onClick={() => RemoveMultipleItems(createTime || key)}
                />}
            </div>
          );
        }
      );
    }
    return null;
  }

  return (
    <div className={styles.multipleItemWrap}>
      {getContent()}
      {multipleItems.length < max && max > 1 && <div onClick={addParams}>
        <PlusSquareOutlined fill="#4168AA" />
        <a>点击新增{typeText}</a>
      </div>}
    </div>
  )
}

export default forwardRef(MultipleItem);
