import React, { useState } from 'react';
import { Select } from 'antd';
import { CaretDownOutlined, CloseOutlined } from '@ant-design/icons';
import { EllipsisTooltip } from '@/components';
import { UpdateUrlParam } from '@/utils/utils';
import styles from './index.less';
import { getProjects } from './service';

const { Option } = Select;

const SelectProTitle = ({ getData, name, type }) => {
  const [selectProFlag, setSelectProFlag] = useState(false);
  const [allProData, setAllProData] = useState([]);

  const SelectFilter = (input, option) => {
    return option.children.props.text.toLowerCase().indexOf(input.toLowerCase()) >= 0;
  };

  const onSelectPro = (v) => {
    if (v) {
      UpdateUrlParam([{ name: 'proId', val: v }]);
      getData && getData(v);
    }
  }

  const onChangeSelect = async () => {
    const { code, data } = await getProjects({
      pageSize: 9999,
      pageNum: 1,
      type
    });
    if (code === 0 && data) setAllProData(data.items);
    setSelectProFlag(true);
  }

  return (
    <div className={styles.selectProTitleWrap}>
      {selectProFlag ? <>
        <Select 
          showSearch 
          filterOption={SelectFilter}
          value={name}
          onChange={onSelectPro}
        >
          {allProData.map((i) => (
            <Option key={i.id} value={i.id}>
              <EllipsisTooltip text={i.name} />
            </Option>
          ))}
        </Select><CloseOutlined onClick={() => setSelectProFlag(false)} /></> :
        <a onClick={onChangeSelect}>{name}<CaretDownOutlined /></a>
      }
    </div>
  )
}

export default (SelectProTitle);
