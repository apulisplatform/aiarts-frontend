import request from '@/utils/request';

export async function getProjects(params) {
  return await request(`/projects/list`, {
    params
  });
}
