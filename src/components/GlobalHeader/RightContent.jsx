import React from 'react';
import { USER_DASHBOARD_PATH } from '@/utils/const';
import { connect } from 'umi';
import { DropdownMenu } from '@apulis/header';
import SelectLang from '../SelectLang';
import Avatar from './AvatarDropdown';
import styles from './index.less';
import NoticeIconView from './NoticeIconView';
import AGroupSelect from '@apulis/group-selector';

const GlobalHeaderRight = (props) => {
  const { theme, layout, platformInfo } = props;
  let className = styles.right;

  if (theme === 'dark' && layout === 'topmenu') {
    className = `${styles.right}  ${styles.dark}`;
  }

  return (
    <div
      className={className}
      style={{ display: 'flex', alignItems: 'center', marginRight: '20px' }}
    >
      <AGroupSelect path="/AIStudio/aiarts/overView" />
      {/* <NoticeIconView /> */}
      {/* <div className={styles.dropdownMenu}>
        <DropdownMenu
          menu={[{
            url: USER_DASHBOARD_PATH,
            text: platformInfo?.iamTitle || JSON.parse(localStorage.getItem('platformConfig'))?.iamTitle
          }]}
        />
      </div> */}
      <Avatar menu />
      <SelectLang className={styles.action} />
    </div>
  );
};

export default connect(({ settings, common }) => ({
  theme: settings.navTheme,
  layout: settings.layout,
  projectName: common.projectName,
  platformInfo: common.platformInfo,
}))(GlobalHeaderRight);
