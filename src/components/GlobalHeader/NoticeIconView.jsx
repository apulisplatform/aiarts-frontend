import React, { useEffect } from 'react';
import { connect } from 'umi';
import NoticeIcon from '../NoticeIcon';
import styles from './index.less';

const mockData = [];
for (let i = 1; i < 20; i++) {
  mockData.push({
    avatar: 'http://linxunzyf.cn/img/avatar.jpg',
    title: '这是标题',
    description: '这是描述信息',
    datetime: Date.now(),
    extra: '这是额外信息',
    key: i,
    read: Math.random() > .5,
  })
}

const NoticeIconView = () => {

  return (
    <NoticeIcon
      className={styles.action}
      count={0}
      onItemClick={(item) => {
        console.log(item);
      }}
      loading={false}
      clearText='清空'
      viewMoreText='查看更多'
      onClear={() => {
        console.log('clear all message')
      }}
      onPopupVisibleChange={visible => {
        console.log(visible);
      }}
      onViewMore={() => window.open(`${window.location.origin}/AIStudio/msg-center/`)}
      clearClose
    >
      <NoticeIcon.Tab
        tabKey='unread'
        count={0}
        list={[]}
        title='未读消息'
        emptyText='您已读完所有消息'
        showViewMore
      />
    </NoticeIcon>
  );
}

export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(NoticeIconView);
