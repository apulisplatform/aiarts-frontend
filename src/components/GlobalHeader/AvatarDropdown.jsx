import { LogoutOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import React from 'react';
import { USER_DASHBOARD_PATH } from '@/utils/const';
import { connect, formatMessage } from 'umi';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';


class AvatarDropdown extends React.Component {
  onMenuClick = (event) => {
    const { key } = event;
    if (key === 'logout') {
      const { dispatch } = this.props;
      if (dispatch) {
        dispatch({
          type: 'login/logout',
        });
      }
    }
    window.open(`${window.location.origin}${USER_DASHBOARD_PATH}/basic/${key === 'setting' ? 'admin/settings' : 'dashboard'}`, "_self");
  };


  render() {
    const {
      currentUser = {
        name: 'User',
      },
      menu,
    } = this.props;

    const menuHeaderDropdown = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={this.onMenuClick}>
        {menu && <>
          <Menu.Item key="center">
            <UserOutlined />
            统一身份认证服务
          </Menu.Item>
          <Menu.Item key="setting">
            <SettingOutlined />
            用户设置
          </Menu.Item>
          <Menu.Divider />
        </>}
        <Menu.Item key="logout">
          <LogoutOutlined />
          {formatMessage({ id: 'component.globalHeader.avatarDropdown.logout' })}
        </Menu.Item>
      </Menu>
    );
    return currentUser && currentUser.userName ? (
      <HeaderDropdown overlay={menuHeaderDropdown} placement="bottomCenter">
        <span className={`${styles.action} ${styles.account}`}>
          <span className={styles.name}>{currentUser.userName}</span>
        </span>
      </HeaderDropdown>
    ) : null;
  }
}

export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(AvatarDropdown);
