import React from 'react';
import { Tooltip } from 'antd';
import { QuestionCircleFilled } from '@ant-design/icons';

const EllipsisTooltip = ({ text, diyContent, isQuestion }) => {
  return (
    <Tooltip placement="bottomLeft" title={text}>
      {isQuestion ? <QuestionCircleFilled /> : 
        diyContent || <span style={{ display:'inline-block', maxWidth: '100%' }} className="textEllipsis">{text}</span>}
    </Tooltip>
  )
}

export default EllipsisTooltip;