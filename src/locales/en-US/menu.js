export default {
  'menu.home': 'Home',
  'menu.overView': 'Overview',
  'menu.project': 'Project',
  'menu.project.list': 'Project List',
  'menu.project.expertDevelop': 'ExpertDevelop',
  'menu.project.modelDevelop': '模型开发',
}