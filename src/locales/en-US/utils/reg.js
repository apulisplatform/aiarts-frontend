export default { 
  "the name can only be composed of Chinese and English, numbers, underscores or horizontal lines": "the name can only be composed of Chinese and English, numbers, underscores or horizontal lines",
  "the name can only start with a lowercase letter and end with a lowercase letter and a number, it is composed of lowercase letters, numbers and a horizontal line below": "the name can only start with a lowercase letter and end with a lowercase letter and a number, it is composed of lowercase letters, numbers and a horizontal line below",
  "the value can only be composed of lowercase letters, numbers, underscores, horizontal lines, dots, or slash": "the value can only be composed of lowercase letters, numbers, underscores, horizontal lines, dots, or slash",
  "reg.input.limit.linuxPath": "reg.input.limit.linuxPath",
  "reg.input.limit.textLength": "reg.input.limit.textLength",
  "reg.input.limit.fileType": "reg.input.limit.fileType",
  "reg.user.path.prefix.reg": "reg.user.path.prefix.reg",
}