import menu from './en-US/menu';
import globalHeader from './en-US/globalHeader';
import { capFirstLetter } from '@/utils/utils';
import Index from './en-US/index.js';
import Project from './en-US/pages/Project';
import AddModalForm from './en-US/pages/Project/addModalForm';
import ExpertDevelop from './en-US/pages/ExpertDevelop';
import DevelopEnvDetail from './en-US/pages/ExpertDevelop/DevelopEnvDetail';
import DevelopEnvForm from './en-US/pages/ExpertDevelop/DevelopEnvForm';
import OtherEnvList from './en-US/pages/ExpertDevelop/OtherEnvList';
import status from './en-US/utils/status';
import ModalForm from './en-US/pages/ExpertDevelop/DevelopEnvDetail/components/ModalForm';

for (const item in menu) {
  menu[item] = capFirstLetter(menu[item]);
}

export default {
  'navBar.lang': 'Languages',
  'layout.user.link.help': 'Help',
  'layout.user.link.privacy': 'Privacy',
  'layout.user.link.terms': 'Terms',
  'app.preview.down.block': 'Download this page to your local project',
  'app.welcome.link.fetch-blocks': 'Get all block',
  'app.welcome.link.block-list': 'Quickly build standard, pages based on `block` development',
  'job.rest.time': 'remaining runnable time',
  'job.rest.minute': 'Min',
  'job.used.time': 'Running Time',
  'download.full.log': 'Download Full Log',
  'download.full.log.loading': 'Loading...',
  'delete.modal.title': 'Confirm delete ?',
  'delete.modal.okText': 'Confirm',
  'delete.modal.content': 'Cannot be restored after deletion',
  'select.file.placeholder': 'Please select',
  'select.file.disable.tip': 'Need to expand the selection file',
  ...menu,
  ...globalHeader,
  ...Index,
  ...Project,
  ...AddModalForm,
  ...ExpertDevelop,
  ...DevelopEnvDetail,
  ...DevelopEnvForm,
  ...OtherEnvList,
  ...status,
  ...ModalForm
};
