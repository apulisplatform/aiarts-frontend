export default {
  "Development Type": "开发类型",
  "Project Name": "项目名称",
  "please enter project name": "请输入项目名称",
  "Project Description": "项目描述",
  "please enter a project description within 200 characters": "请输入描述，限制200字符以内",
}
