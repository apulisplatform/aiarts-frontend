export default {
  "Are you sure to delete this project？": "确定删除该项目吗？",
  "Delete": "删除",
  "Cancel": "取消",
  "delete success！": "删除成功！",
  "Expert Model": "专家模型",
  "Programming Online": "在线编程",
  "预置模型建模": "预置模型建模",
  "零代码实现构建模型训练、评估、推理一站式AI全流程": "零代码实现构建模型训练、评估、推理一站式AI全流程",
  "Project Name": "项目名称",
  "Development Type": "开发类型",
  "Creator": "创建人",
  "Description": "描述",
  "Create Time": "创建时间",
  "Action": "操作",
  "create success！": "创建成功！",
  "please enter project name or creator": "请输入项目名称、创建人进行搜索",
  "New Project": "新建项目",
  "Submit": "提交",
}
