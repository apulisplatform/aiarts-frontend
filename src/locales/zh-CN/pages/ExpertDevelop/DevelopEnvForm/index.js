export default { 
  "please select image type": "请选择镜像类型",
  "please select single card resource specification": "请选择资源规格",
  "please select card number": "请选择卡数",
  "dataset": "数据集",
  "please select node number": "请选择节点数量",
  "image source": "镜像来源",
  "image center": "镜像中心",
  "external image": "外部镜像",
  "image type": "镜像类型",
  "please enter image type": "请输入镜像类型",
  "please enter public image access path in dockerhub": "请输入dockerhub中的公开镜像访问路径",
  "import datasets from ADHub and add up to 5 datasets": "从ADHub中导入数据集，最多添加5个",
  "resource type": "资源类型",
  "resource use": "资源使用",
  "node number": "节点数量",
  "When the number of nodes exceeds one, the resource usage will be scheduled on multiple nodes, and the development environment will become a distributed environment": "当节点数量超过1个，资源使用将会调度在多个节点上，开发环境将变成分布式环境",
}