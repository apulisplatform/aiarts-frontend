export default {
  "reload success": "刷新成功",
  "Project": "项目",
  "Expert Develop": "专家开发",
  "Code Develop": "代码开发",
  "Model Training": "模型训练",
  "Model Develop": "预置模型开发",
  "Model Factory": "模型工厂",
  "Inference Center": "推理中心",
  "Resource Type": "资源类型",
  "Resource Model": "资源型号",
  "Resource Number(used / available)": "资源数量（已用/可用）",
  "start success！": "启动成功！",
  "There is no development environment in this group": "该组内没有开发环境",
  "develop environment": "开发环境",
  "start develop environment": "启动开发环境",
  "User group resource": "所属用户组资源",
  "group develop environment": "组内开发环境",
  "reload resource": "刷新资源",
  "New Project": "新建项目",
  "Please create a project first": "请先到项目列表页创建一个项目",
  "return project list": "返回项目列表",
  "description": "描述",
}
