export default {
  'menu.home': '首页',
  // -----sidebar-----
  'menu.login': '登录',
  'menu.register': '注册',
  'menu.register.result': '注册结果',
  'menu.exception.403': '403',
  'menu.exception.404': '404',
  'menu.exception.500': '500',
  'menu.account': '个人页',
  'menu.account.center': '个人中心',
  'menu.account.settings': '个人设置',
  'menu.account.trigger': '触发报错',
  'menu.account.logout': '退出登录',

  'menu.overView': '总览',
  'menu.project': '项目管理',
  'menu.project.list': '项目列表',
  'menu.project.expertDevelop': '代码开发',
  'menu.project.modelDevelop': '模型训练',
};
