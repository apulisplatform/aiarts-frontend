export default { 
  "the name can only be composed of Chinese and English, numbers, underscores or horizontal lines": "名称只能由中英文，数字，下划线或横线组成",
  "the name can only start with a lowercase letter and end with a lowercase letter and a number, it is composed of lowercase letters, numbers and a horizontal line below": "名称只能由小写字母开头，以小写字母和数字结尾，小写字母，数字，横线组成",
  "the value can only be composed of lowercase letters, numbers, underscores, horizontal lines, dots, or slash": "只能由小写字母，数字，下划线，横线，点，或者斜杠组成",
  "reg.input.limit.linuxPath": "_",
  "reg.input.limit.textLength": "_",
  "reg.input.limit.fileType": "_",
  "reg.user.path.prefix.reg": "_",
}