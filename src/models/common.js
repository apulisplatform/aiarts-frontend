import { getPlatformConfig } from '../services/common';

export const locales = ['zh-CN', 'en-US'];

const common = {
  namespace: 'common',
  state: {
    interval: localStorage.interval === 'null' ? null : Number(localStorage.interval) || 3000,
    platformName: '',
    i18n: locales.includes(localStorage.language) ? localStorage.language : navigator.language,
    platformInfo: '',
  },
  effects: {
    * fetchPlatformConfig({ payload }, { call, put }) {
      const { code, data } = yield call(getPlatformConfig);
      if (code === 0) {
        // res.i18n = 'en-US'; // 开发
        // if (locales.includes(res.i18n)) {
        //   setI18n(res.i18n);
        // }
        yield put({
          type: 'savePlatform',
          payload: {
            platformName: '人工智能平台AIArts',
            platformInfo: data,
            // i18n: res.i18n,
          },
        });
      }
    },
  },
  reducers: {
    savePlatform(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};

export default common;
