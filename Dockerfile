FROM node:12


RUN mkdir -p /home/aiartsFrontend
WORKDIR /home/aiartsFrontend
ADD package.json .
ADD package-lock.json .
ADD yarn.lock .
# RUN npm config set registry 'https://registry.npm.taobao.org'
RUN npm install

COPY . /home/aiartsFrontend

RUN npm run build


FROM node:12-alpine
RUN mkdir -p /home/app/dist && mkdir -p /home/app/server
WORKDIR /home/app/server
COPY --from=0 /home/aiartsFrontend/dist ../dist
COPY --from=0 /home/aiartsFrontend/server .
# RUN yarn config set registry 'https://registry.npm.taobao.org'
RUN yarn

EXPOSE 80

CMD ["node", "index"]
