const AIArtsPrefix = '/ai-arts/api/v1';

export default [
  {
    path: '/',
    component: '../layouts/SecurityLayout',
    routes: [
      {
        path: '/',
        redirect: '/overView',
      },
      {
        path: '/',
        component: '../layouts/BasicLayout',
        routes: [
          {
            path: '/overView',
            name: 'overView',
            icon: 'HomeOutlined',
            component: './index.jsx',
            authority: {
              anyOf: [
                [AIArtsPrefix + '/projects/list', 'get']
              ],
            },
          },
          {
            path: '/project',
            name: 'project',
            icon: 'ReadOutlined',
            routes: [
              {
                path: '/project/list',
                name: 'list',
                component: './Project',
                authority: {
                  anyOf: [
                    [AIArtsPrefix + '/projects/list', 'get']
                  ],
                },
              },
              {
                path: '/project/expertDevelop',
                name: 'expertDevelop',
                component: './ExpertDevelop',
                authority: {
                  anyOf: [
                    [AIArtsPrefix + '/projects/{id}', 'get']
                  ],
                },
              },
              {
                path: '/project/modelDevelop/',
                name: 'modelDevelop',
                authority: {
                  anyOf: [
                    [AIArtsPrefix + '/projects/{id}', 'get']
                  ],
                },
                routes: [
                  {
                    path: '/project/modelDevelop',
                    component: './ExpertDevelop',
                    name: '模型训练',
                    hideInMenu:true,
                    authority: {
                      anyOf: [
                        [AIArtsPrefix + '/projects/{id}', 'get']
                      ],
                    },
                  },
                  {
                    path: '/project/modelDevelop/train',
                    component: './ModelDevelop/Train',
                    hideInMenu:true,
                    authority: {
                      anyOf: [
                        [AIArtsPrefix + '/projects/{id}', 'get']
                      ],
                    },
                  },
                  {
                    path: '/project/modelDevelop/release',
                    component: './ModelDevelop/Release',
                    hideInMenu:true,
                    authority: {
                      anyOf: [
                        [AIArtsPrefix + '/projects/{id}', 'get']
                      ],
                    },
                  },
                ]
              }
            ]
          },
          {
            component: './404',
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
];
