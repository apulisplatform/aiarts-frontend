export default {
  navTheme: 'dark',
  // 拂晓蓝
  primaryColor: '#4168AA',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: false,
  autoHideHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: 'aiarts',
  pwa: false,
  iconfontUrl: '',
};
